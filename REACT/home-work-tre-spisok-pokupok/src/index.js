/* Описание Домашнего задания №3:
Необходимо створити додаток - список покупок 
+ Кнопка "додати" - " Відкриває інпут і кнопку "зберегти"
+ де ми вводимо нову позицію 
+ після натиску "зберегти" має створитись нова позиція і виглядати так: 
  Виводити список покупок з можливістю редагування. 
  В кожній позиції покупки має бути Назва і знаки
+ кнопка "редагувати" (Редагує позицію)
+ кнопка "видалити" (Видаляє позицію)
+ кнопка "куплено" (Закреслює назву позиції)
Тема: Два вида компонентов (функциональная и классовая). Объект state. Свойство key. События элементов.
*/

import React from "react";
import ReactDom from "react-dom";
import Main from "./components/main";



ReactDom.render(<Main></Main>, document.querySelector(".body"));
