// Вариант функциональной компоненты:
/*
import Button from "../button";

import { clickAdd, clickSohranit, spisokLocalStorage, redaktirovanie, slychainoeZnachenie, udalenie, pokupka } from "../../functions";

import "./main.css";

let arrSpisok = spisokLocalStorage();

function Main() {

    return (
        <main>
            <div className="main">

                <Button class="dobav" value="Додати ➕" events={clickAdd}></Button>

                <div className="sozdat">
                    <input type="text" placeholder="купить..." className="input" />
                    <Button class="sohranit" value="Зберегти 📀" events={clickSohranit}></Button>
                </div>

                <div className="spisok">
                    {arrSpisok.map((value) => {
                        return (
                            <div key={slychainoeZnachenie()}>
                                <p className="tovar">{value}</p>
                                <Button class="diyaStovarom" value="✍️" events={redaktirovanie} atrForFunction={value}></Button>
                                <Button class="diyaStovarom" value="❌" events={udalenie} atrForFunction={value}></Button>
                                <Button class="diyaStovarom" value="✅" events={pokupka} atrForFunction={value}></Button>
                            </div>
                        )
                    })}
                </div>

            </div>
        </main>
    );
}
*/

// Вариант классовой компоненты:

import { Component } from "react";
import Button from "../button";

import { slychainoeZnachenie, clickAdd, spisokLocalStorage } from "../../functions";

import "./main.css";

class Main extends Component {

    state = {
        vSpisok: spisokLocalStorage(),
        indexForObnovleniya: 0
    }

    sohrValue = () => {

        let inputValue = document.querySelector(".input").value;
        if (inputValue.length === 0) {
            return;
        } else {
            this.setState((state) => {
                return {
                    ...state,
                    vSpisok: spisokLocalStorage(inputValue)
                }
            })
        }
        document.querySelector(".input").value = "";
    }

    redaktirovanie = (index) => {

        document.querySelector(".sohranit").classList.add("displayNone");
        document.querySelector(".obnovit").classList.add("displayInlineBlock");

        document.querySelector(".sozdat").classList.add("displayBlock");

        let arr = JSON.parse(localStorage.spisok);
        let value = arr[index].valueObj;
        document.querySelector(".input").value = value;

        this.setState((state) => {
            return {
                ...state,
                indexForObnovleniya: index
            }
        })
    }

    obnovlenie = (index) => {

        document.querySelector(".sohranit").classList.remove("displayNone");
        document.querySelector(".obnovit").classList.remove("displayInlineBlock");

        let inputValue = document.querySelector(".input").value;
        if (inputValue.length === 0) {
            return;
        } else {

            let arr = JSON.parse(localStorage.spisok);
            arr[index].valueObj = inputValue;
            let json = JSON.stringify(arr);
            localStorage.spisok = json;

            this.setState((state) => {
                return {
                    ...state,
                    vSpisok: spisokLocalStorage()
                }
            })
        }
        document.querySelector(".input").value = "";
    }

    udalenie = (index) => {

        let arr = JSON.parse(localStorage.spisok);
        arr.splice(index, 1);

        let json = JSON.stringify(arr);
        localStorage.spisok = json;

        this.setState((state) => {
            return {
                ...state,
                vSpisok: spisokLocalStorage()
            }
        })
    }

    pokupka = (index) => {
        
        let arr = JSON.parse(localStorage.spisok);

        if (/textDecor/.test(arr[index].styleObj)) {
            arr[index].styleObj = "tovar";
        } else {
            arr[index].styleObj = "tovar textDecor";
        }
        
        let json = JSON.stringify(arr);
        localStorage.spisok = json;

        this.setState((state) => {
            return {
                ...state,
                vSpisok: spisokLocalStorage()
            }
        })        
    }

    render() {
        return (
            <main>
                <div className="main">

                    <Button value="Додати ➕" nameStyle="dobav" nameFunction={clickAdd}></Button>

                    <div className="sozdat">
                        <input type="text" placeholder="купить..." className="input" />
                        <Button value="Зберегти 📀" nameStyle="sohranit" nameFunction={this.sohrValue}></Button>
                        <Button value="Oновити ✍️" nameStyle="obnovit" nameFunction={this.obnovlenie} atrForFunction={this.state.indexForObnovleniya}></Button>
                    </div>

                    <div className="spisok">

                        {this.state.vSpisok.map((obj, index) => {
                            return (
                                <div key={slychainoeZnachenie()}>
                                    <p className={obj.styleObj}>- {obj.valueObj}</p>
                                    <Button value="✍️" nameStyle="diyaStovarom" nameFunction={this.redaktirovanie} atrForFunction={index}></Button>
                                    <Button value="❌" nameStyle="diyaStovarom" nameFunction={this.udalenie} atrForFunction={index}></Button>
                                    <Button value="✅" nameStyle="diyaStovarom" nameFunction={this.pokupka} atrForFunction={index}></Button>
                                </div>
                            )
                        })}

                    </div>

                </div>
            </main >
        );
    }
}

export default Main;

