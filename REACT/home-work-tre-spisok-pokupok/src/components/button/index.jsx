// Вариант для функциональной компоненты:
/*
import "./button.css";

function Button(props) {
    return (
        <button type="button" className={props.class} onClick={() => {
            props.atrForFunction === "" ? props.events() : props.events(props.atrForFunction)}}>
            {props.value}
        </button>
    )
}

export default Button;
*/

// Вариант для классовой компоненты:

import "./button.css";

function Button({ value, nameStyle, nameFunction, atrForFunction }) {

    return (

        <button type="button" className={nameStyle} onClick={() => { 
            atrForFunction === "" ? nameFunction() : nameFunction(atrForFunction)}}>
        {value}
        </button>
    )
}

export default Button;