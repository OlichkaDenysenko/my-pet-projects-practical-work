// Вариант для классовой компоненты:

let slychainoeZnachenie = () => {
    let alfavit = ['A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z'];
    let one = Math.floor(Math.random() * alfavit.length);
    let two = Math.floor(Math.random() * alfavit.length);
    let tre = Math.floor(Math.random() * alfavit.length);
    let treBukv = alfavit[one] + alfavit[two] + alfavit[tre];

    let chislo = Math.floor(Math.random() * 100 + 1);
    let znachenie = chislo + treBukv;
    return znachenie;
}

let clickAdd = () => {

    document.querySelector(".sozdat").classList.add("displayBlock");
}

let spisokLocalStorage = (value) => {

    if (!value) {
        if (!localStorage.spisok || localStorage.spisok === undefined) {
            let arr = [];
            let json = JSON.stringify(arr);
            localStorage.spisok = json;
            return arr;
        }

        if (localStorage.spisok) {
            let arr = JSON.parse(localStorage.spisok);
            return arr;
        }

    } else {
        if (!localStorage.spisok || localStorage.spisok === undefined) {
            let arr = [];
            let json = JSON.stringify(arr);
            localStorage.spisok = json;
            return arr;
        }

        if (localStorage.spisok) {
            let arr = JSON.parse(localStorage.spisok);

            let obj = {
                valueObj: value,
                styleObj: "tovar"
            }

            arr.push(obj);
            let json = JSON.stringify(arr);
            localStorage.spisok = json;
            return arr;
        }
    }
}

export { slychainoeZnachenie, clickAdd, spisokLocalStorage };

// Вариант для функциональной компоненты:
/*
let slychainoeZnachenie = () => {
    let alfavit = ['A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z'];
    let one = Math.floor(Math.random() * alfavit.length);
    let two = Math.floor(Math.random() * alfavit.length);
    let tre = Math.floor(Math.random() * alfavit.length);
    let treBukv = alfavit[one] + alfavit[two] + alfavit[tre];

    let chislo = Math.floor(Math.random() * 100 + 1);
    let znachenie = chislo + treBukv;
    return znachenie;
}

let spisokLocalStorage = (value) => {

    if (!value) {
        if (!localStorage.spisok || localStorage.spisok === undefined) {
            let arr = [];
            let json = JSON.stringify(arr);
            localStorage.spisok = json;
            return arr;
        }

        if (localStorage.spisok) {
            let arr = JSON.parse(localStorage.spisok);
            return arr;
        }

    } else {
        if (!localStorage.spisok || localStorage.spisok === undefined) {
            let arr = [];
            let json = JSON.stringify(arr);
            localStorage.spisok = json;
            return arr;
        }

        if (localStorage.spisok) {
            let arr = JSON.parse(localStorage.spisok);
            arr.push(value);
            let json = JSON.stringify(arr);
            localStorage.spisok = json;
            return arr;
        }
    }
}

let clickAdd = () => {

    document.querySelector(".sozdat").classList.add("displayBlock");
}

let clickSohranit = () => {

    let inputValue = `- ${document.querySelector(".input").value}`;

    if (inputValue.length <= 2) {
        return;
    } else {
        spisokLocalStorage(inputValue);
        window.location.reload();
    }
}

let poisk = (value, diya) => {

    let arr = JSON.parse(localStorage.spisok);
    let newArr;

    if (diya === "redact") {

        newArr = arr.filter((val) => {
            return val !== value;
        })

        document.querySelector(".sozdat").classList.add("displayBlock");

        let newVelue = value.substr(2);
        document.querySelector(".input").value = newVelue;

        let json = JSON.stringify(newArr);
        localStorage.spisok = json;
    }
    if (diya === "udalit") {

        newArr = arr.filter((val) => {
            return val !== value;
        })

        let json = JSON.stringify(newArr);
        localStorage.spisok = json;

        window.location.reload();
    }
}

let redaktirovanie = (value) => {
    poisk(value, "redact");
}

let udalenie = (value) => {
    poisk(value, "udalit");
}

let pokupka = (value) => {

    let [...p] = document.querySelectorAll(".tovar");
    p.forEach((vseP) => {

        if (vseP.innerText === value) {
            vseP.classList.add("textDecor");
        }
    })
}

export { clickAdd, clickSohranit, spisokLocalStorage, redaktirovanie, slychainoeZnachenie, udalenie, pokupka };
*/