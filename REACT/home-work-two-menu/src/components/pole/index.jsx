// дочерний компонент
import React from "react";

import "./pole.css";

function Pole(props) {
    return (
        <div className="pole">

            <div className="iconka">
                <img src={props.src} alt={props.alt} />
            </div>

            <div className="opisanie">{props.opisanie}</div>
            
        </div>
    );
}

export default Pole;