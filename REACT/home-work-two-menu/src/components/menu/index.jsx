// родительский компонент
import React from "react";

import vpered from "../../img/vpered.svg";
import nazad from "../../img/nazad.svg";
import luna from "../../img/luna.svg";
import solnse from "../../img/solnse.svg";

import analitika from "../../img/analitika.svg";
import domik from "../../img/domik.svg";
import kolokolchik from "../../img/kolokolchik.svg";
import korobochka from "../../img/korobochka.svg";
import palochki from "../../img/palochki.svg";
import poisk from "../../img/poisk.svg";
import vihod from "../../img/vihod.svg";

import Pole from "../pole";

import "./menu.css";

function Menu() {
    return (
        <div className="menu">

            <div>           
                <div className="shapka-menu">
                    <div className="block-iconka">AF</div>
                    <div className="opisanie">
                        <div className="zagolovok">AnimatedFred</div>
                        <div className="email">animated@demo.com</div>
                    </div>
                    <img
                        className="pozishion vpered"
                        src={vpered}
                        alt="vpered"
                    />
                    <img
                        className="pozishion nazad"
                        src={nazad}
                        alt="nazad"
                    />
                </div>

                <Pole src={poisk} alt={"search"} opisanie={"Search..."}></Pole>
                <Pole src={domik} alt={"dashboard"} opisanie={"Dashboard"}></Pole>
                <Pole src={palochki} alt={"revenue"} opisanie={"Revenue"}></Pole>
                <Pole src={kolokolchik} alt={"notifications"} opisanie={"Notifications"}></Pole>
                <Pole src={analitika} alt={"analytics"} opisanie={"Analytics"}></Pole>
                <Pole src={korobochka} alt={"inventory"} opisanie={"Inventory"}></Pole>
            </div>

            <div>

                <Pole src={vihod} alt={"logout"} opisanie={"Logout"}></Pole>

                <div className="footer-menu">
                    <div className="iconka">
                        <img src={solnse} alt="light" />
                    </div>
                    <div className="opisanie">Light mode</div>
                    <div className="perekluchatel">
                        <img src={luna} alt="perekluchatel" />
                    </div>
                </div>

            </div>
        </div>

    );
}

export default Menu;