/* Описание Домашнего задания №1:
Необходимо создать табличку, в которой будут перечислены знаки зодиака и их даты.
Тема: ознакомление с React - начиная с версии 16-17
*/

import React from 'react';
import ReactDOM from 'react-dom';

function Znaki() {
    return (
        <>
            <p>Знаки зодиака по датам:</p>

            <table>
                <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Знак зодиака</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>21 марта – 19 апреля</td>
                        <td>Овен</td>
                    </tr>
                    <tr>
                        <td>21 апреля – 21 мая</td>
                        <td>Телец</td>
                    </tr>
                    <tr>
                        <td>22 мая – 21 июня</td>
                        <td>Близнецы</td>
                    </tr>
                    <tr>
                        <td>22 июня – 22 июля</td>
                        <td>Рак 🦞</td>
                    </tr>
                    <tr>
                        <td>23 июля – 21 августа</td>
                        <td>Лев</td>
                    </tr>
                    <tr>
                        <td>22 августа – 23 сентября</td>
                        <td>Дева</td>
                    </tr>
                    <tr>
                        <td>24 сентября – 23 октября</td>
                        <td>Весы</td>
                    </tr>
                    <tr>
                        <td>24 октября – 22 ноября</td>
                        <td>Скорпион</td>
                    </tr>
                    <tr>
                        <td>23 ноября – 22 декабря</td>
                        <td>Стрелец</td>
                    </tr>
                    <tr>
                        <td>23 декабря – 20 января</td>
                        <td>Козерог</td>
                    </tr>
                    <tr>
                        <td>21 января – 19 февраля</td>
                        <td>Водолей</td>
                    </tr>
                    <tr>
                        <td>20 февраля – 20 марта</td>
                        <td>Рыбы</td>
                    </tr>
                </tbody>
            </table>
        </>
    );
}

ReactDOM.render(<Znaki></Znaki>, document.querySelector(".konteiner"));