/* Описание:
Домашняя работа №13.
Необходимо:
+ 1. Добавить слайдер на сайт піцерії з перемиканням слайдів за 3с автоматически, прописаный с помощью библиотек jQuery и Вootstrap
+ 2. Задача на прототипи, використовуючи можливості стандарту ES5 (Технічні вимоги в файле: homework.js)
Тема: слайдер из jQuery и Вootstrap, конструктор ES5 (Hamburger)
*/

// Завдання #1:
//Добавить слайдер на сайт піцерії з перемиканням слайдів за 3с автоматически, прописаный с помощью библиотек jQuery и Вootstrap

let d = $(".carousel-item").attr("data-interval", "3000");

//Завдання #2:
//Написати реалізацію функції, яка дозволить створювати об'єкти на кшталт Hamburger, використовуючи можливості стандарту ES5.

//информация о выявленной ошибке во время работы:
function HamburgerException(sms) {

    if (!sms) {
        console.log("Ошибок не выявлено!");
    } else {
        console.log(sms);
    }
}

//основной конструктор по созданию гамбургера:
function Hamburger(size, stuffing) {

    try {

        if (size.search("SIZE_SMALL") !== -1 || size.search("SIZE_LARGE") !== -1) {
            this.size = size;
        } else {
            alert(`Введите корректный запрос размера: SIZE_SMALL - маленький, SIZE_LARGE - большой `);
            this.size = error;       //вызовем ошибку     
        }

    } catch (error) {
        throw new Error(HamburgerException(`${error.message}. Invalid size:"${size}". Ошибка при вводе размера Гамбургера.`));
    }

    try {

        if (stuffing.search("STUFFING_CHEESE") !== -1 || stuffing.search("STUFFING_SALAD") !== -1 || stuffing.search("STUFFING_POTATO") !== -1) {
            this.stuffing = stuffing;
        } else {
            alert(`Введите корректный запрос начинки: STUFFING_CHEESE - с сыром, STUFFING_SALAD - с салатом, STUFFING_POTATO - с картофелем.`);
            this.stuffing = error;       //вызовем ошибку     
        }

    } catch (error) {
        throw new Error(HamburgerException(`${error.message}. No size given. Ошибка при вводе начинки Гамбургера.`));
    }
}

Hamburger.SIZE_SMALL = {
    price: 50,
    colorii: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    colorii: 40
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    colorii: 20
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    colorii: 5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    colorii: 10
};
Hamburger.TOPPING_MAYO = {
    price: 20,
    colorii: 5
};
Hamburger.TOPPING_SPICE = {
    price: 15,
    colorii: 0
};

// основа собранного гамбургера:
Hamburger.prototype.plus = function () {

    let forSummirovaniya = [];
    let kaloriy = [];
    Hamburger.number = [];
    Hamburger.numberKalorii = [];

    //найти свойства из конструктора
    for (let svoystvo in Hamburger) {

        if (svoystvo.search(this.size) !== -1) {

            forSummirovaniya.push(Hamburger[svoystvo].price);
            kaloriy.push(Hamburger[svoystvo].colorii);

            Hamburger.number.push(Hamburger[svoystvo].price);
            Hamburger.numberKalorii.push(Hamburger[svoystvo].colorii);
        }

        if (svoystvo.search(this.stuffing) !== -1) {

            forSummirovaniya.push(Hamburger[svoystvo].price);
            kaloriy.push(Hamburger[svoystvo].colorii);

            Hamburger.number.push(Hamburger[svoystvo].price);
            Hamburger.numberKalorii.push(Hamburger[svoystvo].colorii);
        }
    }

    //суммировать:
    let rezPrice = forSummirovaniya.reduce((a, b) => {
        a += b;
        return a;
    }, 0);

    let rezKaloriy = kaloriy.reduce((a, b) => {
        a += b;
        return a;
    }, 0);

    //высветить результат заказа в console:
    //console.log(`Стоимость гамбургера: ${rezPrice} ₮.`);
    //console.log(`Количество калорий: ${rezKaloriy} ккал.`);    
}

//добавить добавки:
Hamburger.prototype.addTopping = function (...topping) {

    if (topping[0] === topping[1]) {

        alert(`Вы можете выбрать вторую добавку при условии, что они будут разные. Пожалуйста повторите выбор, а пока оформим только одну добавку: "${topping[0]}", которая выбрана Вами в первую очередь.`);

        HamburgerException(`Duplicate topping: "${topping[0]}"`);

        let f = [];
        f.push(topping[0]);
        return f;

    } else {

        let forSummirovaniya = [];
        let kaloriy = [];

        try {

            topping.forEach((top) => {

                if (top.search("TOPPING_MAYO") !== -1 || top.search("TOPPING_SPICE") !== -1) {

                    //найти свойства из конструктора
                    for (let svoystvo in Hamburger) {

                        if (svoystvo.search(top) !== -1) {

                            forSummirovaniya.push(Hamburger[svoystvo].price);
                            kaloriy.push(Hamburger[svoystvo].colorii);
                        }
                    }

                } else {
                    alert(`Введите корректный запрос добавки: TOPPING_MAYO - майонез, TOPPING_SPICE - приправы.`);
                    topping = error;      //вызовем ошибку     
                }
            })

        } catch (error) {
            throw new Error(HamburgerException(`${error.message}. Ошибка при вводе добавки Гамбургера.`));
        }

        //суммировать:
        let rezPrice = forSummirovaniya.reduce((a, b) => {
            a += b;
            return a;
        }, 0);

        let rezKaloriy = kaloriy.reduce((a, b) => {
            a += b;
            return a;
        }, 0);

        return topping;  //чтобы воспользоваться этой инфо
    }

}

//добавить ещё добавки:
Hamburger.prototype.removeTopping = function (topping) {

    let forSummirovaniya = [];
    let kaloriy = [];
    Hamburger.TOPPING_ = [];

    //найти свойства из конструктора для уже ранее выбранных добавок
    dobfvka.forEach((top) => {
        for (let svoystvo in Hamburger) {

            if (svoystvo.search(top) !== -1) {

                forSummirovaniya.push(Hamburger[svoystvo].price);
                kaloriy.push(Hamburger[svoystvo].colorii);

                Hamburger.TOPPING_.push(top);
                Hamburger.number.push(Hamburger[svoystvo].price);
                Hamburger.numberKalorii.push(Hamburger[svoystvo].colorii);
            }
        }
    })

    //найти свойства из конструктора для дополнительной добавки
    try {

        if (topping.search("TOPPING_MAYO") !== -1 || topping.search("TOPPING_SPICE") !== -1) {

            //найти свойства из конструктора
            for (let svoystvo in Hamburger) {

                if (svoystvo.search(topping) !== -1) {

                    forSummirovaniya.push(Hamburger[svoystvo].price);
                    kaloriy.push(Hamburger[svoystvo].colorii);

                    Hamburger.TOPPING_.push(topping);
                    Hamburger.number.push(Hamburger[svoystvo].price);
                    Hamburger.numberKalorii.push(Hamburger[svoystvo].colorii);
                }
            }

        } else {
            alert(`Введите корректный запрос добавки: TOPPING_MAYO - майонез, TOPPING_SPICE - приправы.`);
            topping = error;      //вызовем ошибку     
        }

    } catch (error) {
        throw new Error(HamburgerException(`${error.message}. Ошибка при вводе дополнительной добавки Гамбургера.`));
    }

    //суммировать:
    let rezPrice = forSummirovaniya.reduce((a, b) => {
        a += b;
        return a;
    }, 0);

    let rezKaloriy = kaloriy.reduce((a, b) => {
        a += b;
        return a;
    }, 0);

    //высветить результат заказа в console:
    //console.log(`Стоимость всех добавок: ${rezPrice} грн.`);
    //console.log(`Количество калорий: ${rezKaloriy} ккал.`);        
}

//получить информацию в console:

//высветить список добавок в console:
Hamburger.prototype.getToppings = function () {

    Hamburger.TOPPING_.forEach((top) => {

        for (let svoystvo in Hamburger) {

            if (svoystvo.search(top) !== -1) {

                console.log(`Вы выбрали добавку к гамбургеру: "${top}", стоимостью: ${Hamburger[svoystvo].price} ₮., в котором ${Hamburger[svoystvo].colorii} колорий.`);
            }
        }
    })
}

//высветить размер гамбургера в console:
Hamburger.prototype.getSize = function () {

    for (let svoystvo in Hamburger) {

        if (svoystvo.search(this.size) !== -1) {

            console.log(`Вы выбрали размер гамбургера: "${this.size}", стоимостью: ${Hamburger[svoystvo].price} ₮., в котором ${Hamburger[svoystvo].colorii} колорий.`);
        }
    }
}

//высветить начинку гамбургера в console:
Hamburger.prototype.getStuffing = function () {

    for (let svoystvo in Hamburger) {

        if (svoystvo.search(this.stuffing) !== -1) {

            console.log(`Вы выбрали начинку гамбургера: "${this.stuffing}", стоимостью: ${Hamburger[svoystvo].price} ₮., в котором ${Hamburger[svoystvo].colorii} колорий.`);
        }
    }
}

//высветить цену гамбургера в console:
Hamburger.prototype.calculatePrice = function () {

    //суммировать:
    let rezPrice = Hamburger.number.reduce((a, b) => {
        a += b;
        return a;
    }, 0);

    console.log(`Общая стоимость гамбургера: ${rezPrice} ₮.`);
}

//высветить калории гамбургера в console:
Hamburger.prototype.calculateCalories = function () {

    //суммировать:
    let rez = Hamburger.numberKalorii.reduce((a, b) => {
        a += b;
        return a;
    }, 0);

    console.log(`Общее количество калориев в гамбургере: ${rez} ккал.`);
}

//прописать заказ:
let zakaz = new Hamburger("SIZE_SMALL", "STUFFING_POTATO");
zakaz.plus();

//добавить добавки (можно несколько):
let dobfvka = zakaz.addTopping("TOPPING_MAYO");

//добавить ещё добавки:
zakaz.removeTopping("TOPPING_SPICE");

//получить информацию в console:

//получить список добавок:
//zakaz.getToppings();

//узнать про размер гамбургера:
//zakaz.getSize();

//узнать про начинку гамбургера:
//zakaz.getStuffing();

//узнать цену гамбургера:
//zakaz.calculatePrice();

//узнать калории гамбургера:
//zakaz.calculateCalories();

// Далее относится к Домашней работе №9:

const date = `© 2002 – ${new Date().getFullYear()} . Мережа піцерій Domino!`;
document.getElementById("address").innerText = date;

// проверка инпутов и сохранение пользователя
let validate = (r, v) => {
    return r.test(v);
};

const user = {
    name: "",
    phone: "",
    email: ""
};

const [...inputsForm] = document.querySelectorAll("#contact-form input");

function inputValidate() {

    //debugger  - остановка для наладчика    

    if (this.type === "text" && validate(/^[А-яіїґєA-z-]+$/i, this.value)) {
        user.name = this.value; // .toLowerCase()
        this.classList.add("success");
        this.classList.remove("error");
    } else if (this.type === "email" && validate(/^[A-z_0-9.]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/, this.value)) {
        user.email = this.value; // .toLowerCase()
        this.classList.add("success");
        this.classList.remove("error");
    } else if (this.type === "tel" && validate(/^\+380[0-9]{9}$/, this.value)) {
        user.phone = this.value;
        this.classList.add("success");
        this.classList.remove("error");
    }
    else {
        this.classList.add("error");
        this.classList.remove("success");
    }
}

inputsForm.forEach((el) => {

    if (el.type === "text" || el.type === "email" || el.type === "tel") {
        el.addEventListener("change", inputValidate);
    }
})

// кнопка "Підтвердити замовлення >>" и проверка формы
const form = document.getElementById("contact-form");
let zakazik = document.getElementById("btnSubmit");

zakazik.addEventListener("click", () => {

    let flag = [];

    inputsForm.forEach((el) => {

        if (el.type === "text" || el.type === "email" || el.type === "tel") {

            if (el.classList.contains("error")) {
                return
            }
            else if (el.value == "") {
                return
            }
            else {
                flag.push("+");
            }
        }
    })

    if (flag.length == 3) {
        document.location = "/final";
    } else {
        console.log("oшибка заполнения полей");
    }
})

// кнопка "Зкинути"
let skinyt = document.getElementById("cancel");
skinyt.addEventListener("click", () => {

    inputsForm.forEach((el) => {

        if (el.type === "text" || el.type === "email" || el.type === "tel") {

            el.classList.remove("error");
            el.classList.remove("success");
        }
    })
})

// создание прайса
class Price {

    constructor() {

        this.size = [{ name: "small", price: 50 }, { name: "mid", price: 75 }, { name: "big", price: 100 }];
        this.sizePrice = null; //вывод цены - при выборе size

        this.sous = [{ name: "Кетчуп", price: 10 }, { name: "BBQ", price: 10 }, { name: "Рiкотта", price: 10 }];
        this.sousesPrice = null; //вывод цены - при выборе соусов

        this.toping = [{ name: "Сир звичайний", price: 15 }, { name: "Сир фета", price: 15 }, { name: "Моцарелла", price: 15 }, { name: "Телятина", price: 20 }, { name: "Помiдори", price: 10 }, { name: "Гриби", price: 15 }];
        this.topingsPrice = null; //вывод цены - при выборе топингов

        this.kolichestvo = null;
    }

    calc() {

        return (this.sizePrice + this.sousesPrice + this.topingsPrice) * this.kolichestvo;
    }

}

let newZakaz = new Price(); // обращение к прайсу

//первоначальный выбор
let itog = document.querySelector(".itog");

let priceShow = document.querySelector(".pri"); // вывести инфо размера
priceShow.innerText = 50;
newZakaz.sizePrice = 50;

let kolichestvoInput = document.querySelector(".input_"); // вывести количествo
kolichestvoInput.value = 1;
newZakaz.kolichestvo = 1;

itog.innerText = newZakaz.calc();

// выбор размера пиццы:
let sizeChoice = document.querySelector("#pizza");

sizeChoice.addEventListener("click", (el) => {

    if (!el.target.closest("span")) {  //если нажал не на <span> - выйти и продолжить
        return;
    }

    let input = el.target.closest("label").firstElementChild;  // добраться до <input> чтобы забрать value
    //console.log(input.value); 

    //перебрать массив size и записать значения
    for (let obj of newZakaz.size) {

        let nameSize = obj.name; //значение name - в виде строки
        let priceSize = obj.price;  //значение price - в виде числа
        //console.log(typeof priceSize);

        if (nameSize.includes(input.value)) { // найти совпадение значения имени из <input> и из массива  
            newZakaz.sizePrice = priceSize;  // записать в прайсе - вывод цены size   

            priceShow.innerText = priceSize;
            itog.innerText = newZakaz.calc();
        }
    }

    console.log(newZakaz.sizePrice);
    //console.log(el.target); // проверить на какой элемент нажато
})

// выбор количества:
kolichestvoInput.addEventListener("change", () => {

    if (kolichestvoInput.value < 1) { // менее 1
        kolichestvoInput.value = 1;
    }
    if (kolichestvoInput.value > 10) { // более 10
        kolichestvoInput.value = 10;
    }

    newZakaz.kolichestvo = kolichestvoInput.value;  // записать в прайсе - количество пицц   
    itog.innerText = newZakaz.calc();
})

// начинки
let [...draggable] = document.querySelectorAll(".draggable");

draggable.forEach((img) => {

    img.addEventListener("dragstart", function (el) {
        el.dataTransfer.setData("text", this.src);
    })
})

// выбор соусов:
let [...sousChoice] = document.querySelectorAll(".sous");

let sumSous = []; // для сложения кoличества выбранных соусов

for (let img of sousChoice) { //перебрать массив картинок соусов

    img.addEventListener("dragend", (el) => { // на каждую картинку повесить событие

        if (!el.target.closest("img")) {  //если нажал не на картинку - выйти и продолжить
            return;
        }

        let span = el.target.nextElementSibling;  // добраться до <span> с инфо

        //перебрать массив sous и записать значения
        for (let obj of newZakaz.sous) {

            let nameSous = obj.name; //значение name - в виде строки
            let priceSous = obj.price;  //значение price - в виде числа
            //console.log(typeof priceSize);

            if (nameSous.includes(span.innerText)) { // найти совпадение значения имени из <span> и из массива  

                sumSous.push(priceSous);  // добавить в массив если выбрать несколько соусов
                let sumirovanie = sumSous.reduce((akk, el) => { return akk + el }) //суммирование елементов массива

                newZakaz.sousesPrice = sumirovanie;  // записать в прайсе - вывод суммы всех соусов 

                let priceShow = document.querySelector(".sau"); // вывести инфо
                priceShow.innerText = sumirovanie;
                itog.innerText = newZakaz.calc();
            }
        }
        //console.log(el.target); // проверить на какой элемент нажато
    })
}

// выбор топингов:
let [...topingChoice] = document.querySelectorAll(".toping");

let sumToping = []; // для сложения кoличества выбранных топингов

for (let img of topingChoice) { // перебрать массив картинок топингов

    img.addEventListener("dragend", (el) => { // на каждую картинку повесить событие

        if (!el.target.closest("img")) {  //если нажал не на картинку - выйти и продолжить
            return;
        }

        let span = el.target.nextElementSibling;  // добраться до <span> с инфо

        //перебрать массив toping и записать значения
        for (let obj of newZakaz.toping) {

            let nameToping = obj.name; //значение name - в виде строки
            let priceToping = obj.price;  //значение price - в виде числа
            //console.log(typeof priceToping);

            if (nameToping.includes(span.innerText)) { // найти совпадение значения имени из <span> и из массива  

                sumToping.push(priceToping);  // добавить в массив если выбрать несколько топингов
                let sumirovanie = sumToping.reduce((akk, el) => { return akk + el }) //суммирование елементов массива

                newZakaz.topingsPrice = sumirovanie;  // записать в прайсе - вывод суммы всех топингов 

                let priceShow = document.querySelector(".top"); // вывести инфо
                priceShow.innerText = sumirovanie;
                itog.innerText = newZakaz.calc();
            }
        }
        // console.log(el.target); // проверить на какой элемент нажато
    })
}

// целевй элемент "пицца"
let pizza = document.querySelector(".table");

pizza.addEventListener("dragover", (e) => {
    if (e.preventDefault) {
        e.preventDefault();
    }
    return false;
})

pizza.addEventListener("drop", function (e) {
    if (e.preventDefault) {
        e.preventDefault();
    }
    if (e.stopPropagation) {
        e.stopPropagation();
    }

    let src = e.dataTransfer.getData("Text");
    let kartinka = document.createElement("img");
    kartinka.src = src;
    this.appendChild(kartinka);

    return false;
})




