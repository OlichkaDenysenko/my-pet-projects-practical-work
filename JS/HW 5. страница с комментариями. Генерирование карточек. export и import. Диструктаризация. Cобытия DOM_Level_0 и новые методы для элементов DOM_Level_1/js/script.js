/* Описание:
Необходимо создать страницу с комментариями.
Тема: Генерирование карточек. export/import. Диструктаризация. Cобытия DOM_Level_0 и новые методы для элементов DOM_Level_1 */

import comments from "./comment.js";

/*
Домашнее задание:
1. создать страницу и стилизовать её
2. создать карточки для комментариев через JS и стилизовать их
3. добавить кнопку с модальным окном из Bootstrap и стилизовать её
4. по клику кнопки - должен высетится комментарий с возможностью редактирования (пока без сохранения)
5. обработать инфо и заполнить в карточки (имя, почта, тело комментария)
*/

console.log(Array.isArray(comments)); // массив
console.dir(comments);

// Вариант 1 (не получается 4 пункт)

comments.forEach((element) => {

    let cards = `

        <div class="card">
       
           <div class="photo_">
               <img class="photo_ikon" src="./img/contakt.jpg" alt="Контакт"
               title="пользователь оставаил свой комментарий">
           </div>
       
           <div class="content">
       
               <div class="name_contact main_text">${element.name}</div>
       
               <div class="mail_contact main_text">${element.email}</div>
       
               <div class="comment">${element.body}</div>
       
           </div>
       
           <!-- Кнопка-триггер модального окна -->
           <button type="button" class="btn btn-primary button_ main_text" data-toggle="modal"
               data-target="#exampleModal">
               редактирование комментария
           </button>
       
        </div>       
    `;

    document.querySelector(".container").innerHTML += cards;
    //document.querySelector(".modal-body").innerHTML = element.body;

    document.getElementsByTagName("button").onclick = () => {

        document.querySelector(".modal-body").innerHTML = element.body;
    }
});

// Вариант 2

//создание корпуса карточки
// принимает тег, местонахождения, атрибут, значение атрибута и текст для тега

function card(tagName, mesto, atribute, atrebuteZnachenie, text) {

    let el = document.createElement(tagName); // создаёт новый тег
    document.querySelector(mesto).prepend(el);    // указывает место расположения
    el.setAttribute(atribute, atrebuteZnachenie); // назначает атрибут и его значение

    if (text) {   //если есть тект - добавляет его
        el.innerText = text;
    }

    return el;
}

comments.forEach((element) => {

    let { name, email, body } = element;

    let korpus = card("div", ".container", "class", "card");

    let button = card("button", ".card", "class", "btn btn-primary button_ main_text", "редактирование комментария");

    let content = card("div", ".card", "class", "content");

    let comment = card("div", ".content", "class", "comment", body);
    let mail_contact = card("div", ".content", "class", "mail_contact main_text", email);
    let name_contact = card("div", ".content", "class", "name_contact main_text", name);

    let photo = card("div", ".card", "class", "photo_");
    let img = card("img", ".photo_", "class", "photo_ikon");

    // дополнительные атрибуты для картинки

    document.querySelector(".photo_ikon").setAttribute("src", "./img/contakt.jpg");
    document.querySelector(".photo_ikon").setAttribute("alt", "Контакт");
    document.querySelector(".photo_ikon").setAttribute("title", "пользователь уже оставаил свой комментарий");

    // дополнительные атрибуты для кнопки

    document.getElementsByTagName("button")[0].setAttribute("type", "button");
    document.getElementsByTagName("button")[0].setAttribute("data-toggle", "modal");
    document.getElementsByTagName("button")[0].setAttribute("data-target", "#exampleModal");

    button.onclick = (el) => {
        //console.log(el); //разобрать составляющие
        document.querySelector(".modal-body").innerHTML = body;
    };

});