//занимается поиском одного элемента
const find = (selector) => {
    return document.querySelector(selector);
}
//занимается поиском элементов
const findAll = (selector) => {
    return document.querySelectorAll(selector);
}
//занимается созданием элемента + назначается сразу стиль
const create = (element, className) => {
    let el = document.createElement(element);
    el.classList.add(className);
    return el;
}
//посмотреть
const look = (element) => {
    return console.log(element);
}
//разобрать
const analisis = (element) => {
    return console.dir(element);
}
//обработать запрос
async function zapros (url) {

    const loading = find(".loading");
    loading.classList.add("displayFlex");

    const otvet = await fetch(url);
    loading.classList.remove("displayFlex");
    return otvet.json();
}

export {find, findAll, create, look, analisis, zapros};