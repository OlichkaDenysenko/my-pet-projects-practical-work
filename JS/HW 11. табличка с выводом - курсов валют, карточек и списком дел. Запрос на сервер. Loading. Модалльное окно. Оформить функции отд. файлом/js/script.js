/* Описание:
Необходимо создать табличку: с выводом курсов валют, карточек и списком дел
Тема: Запрос на сервер. Loading. Модалльное окно. Оформить функции отд.файлом
*/

/*
Домашнее задание №11:
+ Создать навигационное меню, 
+ который будет показывать один из вариантов: 

 + 1. Курс валют НБУ (с датой) из: https://bank.gov.ua/ua/open-data/api-dev
 + 2. героев звёздных войн из: https://swapi.dev/api/people/ , 
  + где будет 8 страниц и на каждой по 10 шт. карточек. 
  + необходимо оформить пагинацию/кнопку. 
 + 3. список дел из: https://jsonplaceholder.typicode.com/todos , 
  + где будет отмечено - выполнено или нет, 
  + с возможностью редактирования задачи.
  (необязательно сохранять все изменённые данные при перезагрузке)

+ при наведении на кликабельные элементы  - оформить соответствующий курсор
+ при наведении на кликабельные элементы - оформить  hover, transform
+ оформить loader
*/

import { find, findAll, create, look, analisis, zapros } from "./functions.js";

let shapka = find(".shapka");
let konteiner = find(".konteiner");
let next = find(".next");

//очистка контейнера
let [...shapkaChildren] = shapka.children;

//look(shapka.children);

shapkaChildren.forEach((element) => {

    element.addEventListener("click", () => {

        let [...arr] = konteiner.children;

        arr.forEach((el) => {
            el.remove();
        })

        next.classList.add("displayNone");
        next.classList.remove("displayBlock");
    })
});

//курс валют НБУ (с датой)
let kurs = find(".kurs");

let month = () => {

    if ((new Date().getMonth() + 1) < 10) {

        return `0${new Date().getMonth() + 1}`;

    } else {
        return `${new Date().getMonth() + 1}`;
    }
}
let data = `${new Date().getDate()}.` + month() + `.${new Date().getFullYear()}`;

kurs.addEventListener("click", () => {

    zapros("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
        .then((result) => {

            let tablichka = `
            <table class="tablichka-kurs">
    
            <thead>
                <tr>
                    <th colspan = "3" class="shapka-tablichki-kurs">Курс валют на: ${data}</th>
                </tr>
                <tr>
                    <th>№:</th><th>Название валюты:</th><th>Курс:</th>
                </tr>
            </thead>
    
            <tbody>
            ${result.map((obj, index) => {
                return `<tr>
                <th>${index + 1}</th><th>${obj.txt}</th><th>${obj.rate.toFixed(2)}</th>
                </tr> `
            }).join("")
                }             
            </tbody>
    
            </table>`;
            konteiner.insertAdjacentHTML("afterbegin", tablichka);
            //look(result);            
        })
})

//герои звёздных войн 
let geroi = find(".geroi");

function url(url) {

    zapros(url)
        .then((result) => {
         
            //очистка контейнера
            let [...arr] = konteiner.children;
            arr.forEach((el) => {
                el.remove();
            })

            //вывод карточек
            let tablichka = `
              <div class="cards">
                  ${result.results.map((obj) => {
                return `
                <div class="card">
                   <div>Имя: <span>${obj.name}</span></div>
                   <div>Пол: <span>${obj.gender}</span></div>
                   <div>Рост: <span>${obj.height}</span></div>
                   <div>Вес: <span>${obj.mass}</span></div>
                   <div>Цвет кожи: <span>${obj.skin_color}</span></div>
                   <div>Цвет волос: <span>${obj.hair_color}</span></div>
                   <div>Цвет глаз: <span>${obj.eye_color}</span></div>
                </div>`
            }).join("")}                                    
              </div>
              `;
            konteiner.insertAdjacentHTML("afterbegin", tablichka);

            next.classList.remove("displayNone");
            next.classList.add("displayBlock");

            sessionStorage.urls = result.next; /*.next - это св-во из объекта, который возвратился от JSON из указанной ссылки. И этот объект у же назван result*/
            if (result.next == null) {
                sessionStorage.urls = "https://swapi.dev/api/people";
            }

            //look(result);
        })
}

geroi.addEventListener("click", () => {
    url("https://swapi.dev/api/people");
})

next.addEventListener("click", () => {
    url(sessionStorage.urls);
})

//список дел
let dela = find(".dela");

dela.addEventListener("click", () => {

    zapros("https://jsonplaceholder.typicode.com/todos")
        .then((result) => {

            let tablichka = `
            <table class="tablichka-delo">
                
                <thead>
                    <tr>
                        <th>№</th><th>Дело:</th><th>Статус:</th><th>Редактирование:</th>
                    </tr>
                </thead>
                <tbody>
                    ${result.map((obj) => {
                return `
                        <tr>
                        <th class="numbers">${obj.id}</th><th>${obj.title}</th><th>${obj.completed === true ? "&#9989;" : "&#10060;"}</th><th class="redact">&#128736;&#65039;</th>
                        </tr>
                        `
            }).join("")}                   
                </tbody>

            </table>`;
            konteiner.insertAdjacentHTML("afterbegin", tablichka);

            //редактирование 
            let [...redact] = findAll(".redact");

            redact.forEach((button) => {

                button.addEventListener("click", () => {
                    let parent = button.parentElement;

                    let modalka = `
                   <div class="parent-modalka">
                     <div class="modalka">
                         <div class="modalka-shapka">Редактирование</div>
                         <div>Описание:</div>
                         <div class="delo" contenteditable="true">${parent.children[1].innerText}</div>
                         <div><label for="status">Статус задачи: </label><input id="status" type="checkbox"></div>
                         <div><button class="sohr">СОХРАНИТЬ</button> <button class="otm">ОТМЕНА</button></div>
                     </div>
                    </div>
                    `;
                    konteiner.insertAdjacentHTML("afterbegin", modalka);

                    //оформить статус
                    let status = find("#status");

                    if (parent.children[2].innerText == "✅") {
                        status.setAttribute("checked", "checked");
                    }

                    //сохранить редактирование 
                    let sohranit = find(".sohr");
                    let delo = find(".delo");

                    let newText = delo.innerText;
                    let number = parent.children[0].innerText;

                    delo.addEventListener("blur", () => { // изменит данные

                        let newDelo = find(".delo");
                        newText = newDelo.innerText;
                        //look(newText);   
                    })

                    sohranit.addEventListener("click", () => { // сохранит данные                        
                        localStorage.number = number;
                        localStorage.text = newText;
                        localStorage.status = "✅";

                        //перезапишет данные
                        let [...numbers] = findAll(".numbers");

                        for (let el of numbers) {

                            if (el.innerText == localStorage.number) {
                                el.nextElementSibling.innerText = localStorage.text;
                                el.nextElementSibling.nextElementSibling.innerText = localStorage.status;
                            }
                        }
                    })

                    sohranit.addEventListener("click", () => { //закрыть модалку
                        parentModalka.classList.add("displayNone");
                    })

                    //закрыть модалку
                    let otmena = find(".otm");
                    let parentModalka = find(".parent-modalka");

                    otmena.addEventListener("click", () => {
                        parentModalka.classList.add("displayNone");
                    })

                    //look(numbers);
                })
            })

            //analisis(result);
        })
})


