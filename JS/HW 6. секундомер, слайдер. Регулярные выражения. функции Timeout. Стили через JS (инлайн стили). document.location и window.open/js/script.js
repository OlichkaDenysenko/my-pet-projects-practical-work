/* Описание:
Необходимо решить задачи.
Тема: Регулярные выражения. функции Timeout. Стили через JS (инлайн стили). document.location и window.open*/

/*
Домашнее задание №6:
1. + Разобраться, для чего необходим флаг m в регулярных выражениях
2. + Практические задачи (4 шт.) из лекции https://vimeo.com/772776189/8335035c92 (3:08)
*/

/*
практическая задача № 1:
+ создать блок и вывести в консоль 
+ информацию о его margin (150px) 
*/

let div = document.getElementById("flag");
div.style.borderRadius = "50%";

let modal = document.createElement("div");
document.getElementById("flag").append(modal);
modal.classList.add("foto");
modal.style.display = "none";

let img = document.createElement("img");
document.querySelector(".foto").append(img);
img.setAttribute("src", "./img/info.png");
img.setAttribute("alt", "Информация о флаге m в регулярных выражениях");

let b = 0;

document.getElementById("flag").onclick = () => {

    b++;

    if (b % 2) {
        modal.style.display = "block";
        div.style.borderRadius = "0";
        console.log(`вы открыли данную вкладку ${b / 2 + 0.5}-й раз`);

    } else {
        modal.style.display = "none";
        div.style.borderRadius = "50%";
        console.log(`вы закрыли данную вкладку ${b / 2}-й раз`);
    }

    if (b == 1) {
        console.log(`margin данного блока равен = ${getComputedStyle(div)["margin"]}`);
    }
}

/*
практическая задача № 2:
1. + создать секундомер (чёрного цвета, экран болотного цвета)
2. + создать 3 кнопки
3. + при нажатии кнопок- меняется фон: старт - зелёный, стоп - красный, сброс - серый
4. + время показывается (часы:минуты:секунды)
5. + использовать конструкторы и стрелочные функции
*/

let ekra = document.querySelector(".ekran");
ekra.style.backgroundColor = "#1A4412";

let sec = 0;
let min = 0;
let chas = 0;
let timer;
let ostanovka = 0;

// 1 Вариант конструктор ES5:

let time = () => {

    sec++;

    if (sec < 10) {

        document.querySelector(".sek").innerText = `0` + sec;

    } else {
        document.querySelector(".sek").innerText = sec;
    }

    // из 60 секунд в минуту
    if (sec == 20) {  // 20 - это типо 60 - для того чтобы быстрее проверять корректность кода

        document.querySelector(".sek").innerText = `00`;
        min++;

        if (min < 10) {

            document.querySelector(".min").innerText = `0` + min;

        } else {
            document.querySelector(".min").innerText = min;
        }

        sec = 0;
    }

    // из 60 минут в час
    if (min == 20) {  // 20 - это типо 60 - для того чтобы быстрее проверять корректность кода

        document.querySelector(".min").innerText = `00`;
        chas++;

        if (chas < 10) {

            document.querySelector(".chas").innerText = `0` + chas;

        } else {
            document.querySelector(".chas").innerText = chas;
        }

        min = 0;
    }
}

document.getElementById("start").onclick = () => {

    ostanovka++;         // но можно и путём использования флагов Boolean
    ekra.style.backgroundColor = "#276E21";

    if (ostanovka % 2) {
        timer = setInterval(time, 1000);
    } else {
        clearInterval(timer);
        document.querySelector(".sek").innerText = `00`;
        document.querySelector(".min").innerText = `00`;
        document.querySelector(".chas").innerText = `00`;
        sec = 0;
        min = 0;
        chas = 0;
    }
}

document.getElementById("stop").onclick = () => {

    ekra.style.backgroundColor = "#7F1013";
    clearInterval(timer);
}

document.getElementById("reset").onclick = () => {

    ekra.style.backgroundColor = "#696969";

    clearInterval(timer);
    document.querySelector(".sek").innerText = `00`;
    document.querySelector(".min").innerText = `00`;
    document.querySelector(".chas").innerText = `00`;
    sec = 0;
    min = 0;
    chas = 0;
}

// 2 Вариант в конструкторе ES6:

class Sekundamer {

    constructor() {
    }

    static time() {

        sec++;

        if (sec < 10) {

            document.querySelector(".sek").innerText = `0` + sec;

        } else {
            document.querySelector(".sek").innerText = sec;
        }

        // из 60 секунд в минуту
        if (sec == 20) {  // 19- это типо 59, а 20 - это 60

            document.querySelector(".sek").innerText = `00`;
            min++;

            if (min < 10) {

                document.querySelector(".min").innerText = `0` + min;

            } else {
                document.querySelector(".min").innerText = min;
            }

            sec = 0;
        }

        // из 60 минут в час
        if (min == 20) {  // 19- это типо 59, а 20 - это 60

            document.querySelector(".min").innerText = `00`;
            chas++;

            if (chas < 10) {

                document.querySelector(".chas").innerText = `0` + chas;

            } else {
                document.querySelector(".chas").innerText = chas;
            }

            min = 0;
        }
    }

    static start(a) {

        ostanovka++;
        
        ekra.style.backgroundColor = a;
    
        if (ostanovka % 2) {
            timer = setInterval(this.time, 1000);
        } else {
            clearInterval(timer);
            document.querySelector(".sek").innerText = `00`;
            document.querySelector(".min").innerText = `00`;
            document.querySelector(".chas").innerText = `00`;
            sec = 0;
            min = 0;
            chas = 0;
        }

    }

    static stop(b) {
        ekra.style.backgroundColor = b;
        clearInterval(timer);
    }

    static reset(c) {
        ekra.style.backgroundColor = c;
        clearInterval(timer);
        document.querySelector(".sek").innerText = `00`;
        document.querySelector(".min").innerText = `00`;
        document.querySelector(".chas").innerText = `00`;
        sec = 0;
        min = 0;
        chas = 0;
    }

}

document.getElementById("start").onclick = () => {
    Sekundamer.start("#276E21");
}

document.getElementById("stop").onclick = () => {
    Sekundamer.stop("#7F1013");
}

document.getElementById("reset").onclick = () => {
    Sekundamer.reset("#696969");
}


/*
практическая задача № 3:
1. + через JS создать поле input для введения номера телефона в формате: 000-000-00-00
2. + через JS создать кнопку для сохранения
3. + после нажатия кнопки - происходит проверка на правильность ввода
4. + если правильно - оформить зелёным фоном и 
   + используя document.location перевести пользователя на страничку, которая хранит в себе картиночку: https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
5. + если не правильно - отобразить ошибку в <div> к input
*/

function elements(tagName, mesto, nameClass, text) {

    let el = document.createElement(tagName); // создаём элемент    
    document.querySelector(mesto).append(el); // указываем его место положение 
    el.classList.add(nameClass); // назначаем стиль   

    if (text) {   // если есть текст - прописываем
        el.innerText = text;
    }

    return el; // возвращаем элемент
}

function atributs(mesto, atribute, znachenie) { // добавляем дополнительные атрибуты 

    let el = document.querySelector(mesto); //ищем элемент
    el.setAttribute(atribute, znachenie); // назначаем атрибут

    return el; // возвращаем элемент

}

let modalka = elements("div", "#telefon", "modalka");
modalka.style.display = "none";

let fotoKartochka = elements("img", ".modalka", "fotoKartochka");
fotoKartochka = atributs(".fotoKartochka", "src", "./img/images.jpg");
fotoKartochka = atributs(".fotoKartochka", "alt", "Что-то пошло не так");

let vvod = elements("label", "#telefon", "vvod", "Введите номер телефона:");
vvod = atributs(".vvod", "for", "nomer");

let pole = elements("input", "#telefon", "pole");
pole = atributs(".pole", "type", "tel");
pole = atributs(".pole", "id", "nomer");
pole = atributs(".pole", "placeholder", "+38 000 000-00-00");

let knopka = elements("input", "#telefon", "knopka");
knopka = atributs(".knopka", "type", "button");
knopka = atributs(".knopka", "value", "отправить");

let form = atributs("#telefon", "autocomplete", "off");

const shablon = /\+38 \d{3} \d{3}-\d{2}-\d{2}/; // +38 000 000-00-00 - для проверки можно вставлять на страничке

//console.dir(pole.value); // для проверки

knopka.onclick = () => {

    if (shablon.test(pole.value)) {
        // поискала в Гугле:
        window.open('https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg', '_blank');
        // и заменила вместо:
        // document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
        // для возможности открыть страницу в новой вкладке
        pole.style.backgroundColor = "#138808";
        modalka.style.display = "none";
    } else {
        modalka.style.display = "block";
        pole.style.backgroundColor = "#9B111E";
    }
}

/*
практическая задача № 4:
1. + создать слайдер на 5 картинок: с планетами
2. + каждая картинка меняется по-разному: слева-направо или наоборот, меняет размер, стухает и т.д.
3. + картинки должны менятся каждые 3 секунды
*/

// 1 вариант - слайдер картинками через setInterval:

let kartOne = elements("img", "#slaider", "kartOne");
kartOne = atributs(".kartOne", "src", "./img/20180913_zaa_p138_057.jpg");
kartOne = atributs(".kartOne", "alt", "Планета");
kartOne.style.display = "none";

let kartTwo = elements("img", "#slaider", "kartTwo");
kartTwo = atributs(".kartTwo", "src", "./img/4848-4.jpg");
kartTwo = atributs(".kartTwo", "alt", "Планета");
kartTwo.style.display = "none";

let kartTri = elements("img", "#slaider", "kartTri");
kartTri = atributs(".kartTri", "src", "./img/Mercury.jpg");
kartTri = atributs(".kartTri", "alt", "Планета");
kartTri.style.display = "none";

let kartFor = elements("img", "#slaider", "kartFor");
kartFor = atributs(".kartFor", "src", "./img/shutterstock_418733752.jpg");
kartFor = atributs(".kartFor", "alt", "Планета");
kartFor.style.display = "none";

let kartFive = elements("img", "#slaider", "kartFive");
kartFive = atributs(".kartFive", "src", "./img/shutterstock_1450308851-640x360.jpg");
kartFive = atributs(".kartFive", "alt", "Планета");
kartFive.style.display = "none";

let kartinki = [kartOne, kartTwo, kartTri, kartFor, kartFive];

let slider = document.querySelector("#slaider");

//let nextSlid = 0;

setInterval(() => {

    //slider.style.display = kartinki[nextSlid++ % kartinki.length].style.display = "block"; // проявляются по очереди, но остаюся все вместе.

    slider = kartinki[0].style.display = "block";
    slider = kartinki[1].style.display = "none";
    slider = kartinki[2].style.display = "none";
    slider = kartinki[3].style.display = "none";
    slider = kartinki[4].style.display = "none";

}, 3000);

//console.dir(slider.children); // для проверки

setInterval(() => {

    slider = kartinki[0].style.display = "none";
    slider = kartinki[1].style.display = "block";
    slider = kartinki[2].style.display = "none";
    slider = kartinki[3].style.display = "none";
    slider = kartinki[4].style.display = "none";

}, 6000);

setInterval(() => {

    slider = kartinki[0].style.display = "none";
    slider = kartinki[1].style.display = "none";
    slider = kartinki[2].style.display = "block";
    slider = kartinki[3].style.display = "none";
    slider = kartinki[4].style.display = "none";

}, 9000);

setInterval(() => {

    slider = kartinki[0].style.display = "none";
    slider = kartinki[1].style.display = "none";
    slider = kartinki[2].style.display = "none";
    slider = kartinki[3].style.display = "block";
    slider = kartinki[4].style.display = "none";

}, 12000);

setInterval(() => {

    slider = kartinki[0].style.display = "none";
    slider = kartinki[1].style.display = "none";
    slider = kartinki[2].style.display = "none";
    slider = kartinki[3].style.display = "none";
    slider = kartinki[4].style.display = "block";

}, 15000);

// 2 вариант - слайдер картинками через конструктор ES6 и setTimeout:

class Kartinochka {

    constructor(className, src) {
        this.className = className;
        this.src = src;
    }

    sozdanieElement() {

        let el = document.createElement("img");
        el.classList.add(this.className);
        el.setAttribute("src", this.src);
        el.setAttribute("alt", "Планета");
        document.querySelector("#slaider_").append(el);
        el.style.display = "none";

        return el;
    }
}

let kartOne_ = new Kartinochka("kartOne", "./img/20180913_zaa_p138_057.jpg");
let sludeOne = kartOne_.sozdanieElement();

let kartTwo_ = new Kartinochka("kartTwo", "./img/4848-4.jpg");
let sludeTwo = kartTwo_.sozdanieElement();

let kartTri_ = new Kartinochka("kartTri", "./img/Mercury.jpg");
let sludeTri = kartTri_.sozdanieElement();

let kartFor_ = new Kartinochka("kartFor", "./img/shutterstock_418733752.jpg");
let sludeFor = kartFor_.sozdanieElement();

let kartFive_ = new Kartinochka("kartFive", "./img/shutterstock_1450308851-640x360.jpg");
let sludeFive = kartFive_.sozdanieElement();


setTimeout(() => {

    sludeOne.style.display = "block";

}, 0);

setTimeout(() => {

    sludeOne.style.display = "none";
    sludeTwo.style.display = "block";

}, 3000);

setTimeout(() => {

    sludeTwo.style.display = "none";
    sludeTri.style.display = "block";

}, 6000);

setTimeout(() => {

    sludeTri.style.display = "none";
    sludeFor.style.display = "block";

}, 9000);

setTimeout(() => {

    sludeFor.style.display = "none";
    sludeFive.style.display = "block";

}, 12000);

// 3 вариант - слайдер фоном:

let slidert = document.querySelector("#slaider__");
let bb = document.createElement("div");
slidert.append(bb);
bb.classList.add("height");

let fon = ["kartOne", "kartTwo", "kartTri", "kartFor", "kartFive"];

let next = 0;

setInterval(() => {
    bb.classList.add(fon[next++ % fon.length]);
}, 3000);