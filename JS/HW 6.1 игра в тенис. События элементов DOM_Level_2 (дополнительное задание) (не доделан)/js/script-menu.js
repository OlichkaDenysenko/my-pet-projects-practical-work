/* Описание:
Необходимо создать игру в ТЕННИС - не домашнее задание, а дополнительное, если будет время
Тема: самостоятельно разобрать тему - "События элементов DOM_Level_2"

+ создать кнопку "игра в теннис"
+ появляется интерфейс (меню) - 3 кнопки:
+ Играть
- Турнирная таблица 
- появляется окно с результатами: игрок (имя) - кол-во забитых - общий результат
+ инфо
+ окно о разработчике 
*/

//console.log("проверка"); // для проверки
//console.dir(myachik); // для проверки

// модальное окно про разработчика:
let info = document.querySelector(".info");
let modalkaInfo = document.createElement("div");
modalkaInfo.classList.add("modalkaInfo");
modalkaInfo.innerText = `
Информация о разработчике:

Страна: Украина
Разработчик: Денисенко О.А.
Квалификация: студент`;
info.append(modalkaInfo);

info.addEventListener("click", () => {
    modalkaInfo.classList.toggle("display");    
})

