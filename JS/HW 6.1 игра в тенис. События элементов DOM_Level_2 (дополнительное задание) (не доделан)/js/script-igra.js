/* Описание:
Необходимо создать игру в ТЕННИС - не домашнее задание, а дополнительное, если будет время
Тема: самостоятельно разобрать тему - "События элементов DOM_Level_2"

+ счётчик, поле и кнопка старт
подвязать к JS: счётчик (программа проверяет - отбил ли пользователь мяч)

+ поле: ракетки, мяч
подвязать к JS: ракетки (пользователь и комп) и мяч

- подвязать к JS кнопка старт: 
-  начинает (появляется окно с формой, где вводятся имена пользователей, движение мяча и ракеток, кнопка меняется на "стоп")
и останавливает игру (кнопка меняется на "старт", 
-  остановка мяча и ракеток, появляется окно с результатами: игрок (имя) - кол-во забитых - общий результат)
*/

let pole = document.querySelector(".pole");

let myachik = document.querySelector(".myachik");

let raketka_kompa = document.querySelector(".raketka_kompa");
let raketka_polzovatelya = document.querySelector(".raketka_polzovatelya");

let schet_kompa = document.querySelector(".schet_kompa");
let schet_polzovatelya = document.querySelector(".schet_polzovatelya");

let knopka = document.querySelector(".knopka");

// подвязать к JS: ракетки (пользователь и комп) и мяч

// ракетка пользователя (управление мышкой):
pole.addEventListener("mousedown", (mouse) => {

   raketka_polzovatelya.style.top = mouse.offsetY + "px";
   if (mouse.offsetY > 288) {
      raketka_polzovatelya.style.top = 288 + "px";
   }
   //console.log(mouse.offsetY);
})

// ракетка пользователя (управление стрелками вверх-вниз):
document.body.addEventListener("keydown", (el) => {

   let znachenie = raketka_polzovatelya.style.top;

   if (el.key == "ArrowUp") {  // если стрелка вверх

      znachenie = parseInt(znachenie) - 4;
      raketka_polzovatelya.style.top = znachenie + "px";

      //console.log("стрелка вверх. Значение ракетки = " + znachenie);
   }

   if (el.key == "ArrowDown") { // если стрелка вниз

      znachenie = parseInt(znachenie) + 4;
      raketka_polzovatelya.style.top = znachenie + "px";

      //console.log("стрелка сниз. Значение ракетки = " + znachenie);
   }

   if (znachenie <= 0) {
      raketka_polzovatelya.style.top = 0;
   }

   if (znachenie >= 288) {
      raketka_polzovatelya.style.top = 288 + "px";
   }

   //console.log(raketka_polzovatelya.style.top);
   //console.log(el.key);
})

// ракетка компьютера:

// мяч:

// подвязать к JS кнопка старт: 
let modalkaInfo = document.querySelector(".modalkaInfo");

knopka.addEventListener("click", () => { // при нажатии первый раз появляется окно с формой, где вводятся имена пользователей  
   modalkaInfo.classList.add("display");
   //console.log(modalkaInfo);
   modalkaInfo.addEventListener("click", () => {
      modalkaInfo.classList.remove("display");
   })
})

knopka.addEventListener("click", () => { // при нажатии второй раз кнопка меняется на "стоп"

   modalkaInfo.classList.remove("display"); //убрать формочку


   // при нажатии второй раз срабатывает движение мяча:
   let random = Math.floor(Math.random() * 100 + 1); // для выбора логики (каждый раз начинается по другому)
   let top = myachik.style.top;  // координата вертикали
   let left = myachik.style.left; // координата горизонтали

   if (random <= 20) {

      let i = 0;

      if (i < 0) {

         top = top + 4;
         left = left + 4;

         myachik.style.top = top + "px";
         myachik.style.left = left + "px";

         i++;
      }

      if (i > 0) {

         top = top * 2;
         left = left * 2;

         myachik.style.top = top + "px";
         myachik.style.left = left + "px";

      }

      console.log("до 20]");
   }

   if (random > 20 && random <= 40) {
      console.log("от 20 до 40]");
   }

   if (random > 40 && random <= 60) {
      console.log("от 40 до 60]");
   }

   if (random > 60 && random <= 80) {
      console.log("от 60 до 80]");
   }

   if (random > 80 && random <= 100) {
      console.log("от 80 до 100]");
   }

   //console.log(random);
   //console.dir(myachik); 
   //console.log("myachik.style.top = " + myachik.style.top);
   //console.log("myachik.style.left = " + myachik.style.left);
})

knopka.addEventListener("click", () => { // при нажатии третий раз кнопка меняется на "старт"

})




