/* Описание:
Необходимо проработать с формочкой
Тема: События элементов DOM_Level_2 с input - ами. Пользовательский атрибут. Навигация для элемента. sprite-иконки  
*/

import cityes from "./city.js";
//console.log(cityes);

/*
Домашняя работа №8:

Задача №1:
+ Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"

Задача №2:
+ Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. 
+ Скільки символів має бути в інпуті, зазначається в атрибуті data-length. 
+ Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

Задача №3:
Використовуючи бібліотеку bootstrap (в лекции сказали, что можно и без bootstrap) створіть форму у якій запитати у користувача данні.
+ Ім'я, Прізвище (Українською)
+ Список з містами України 
+ Номер телефону у форматі +380XX XXX XX XX - 
+ Визначити код оператора 
+ та підтягувати логотип оператора. 
+ Пошта 
+ Якщо поле має помилку показати червоний хрестик біля поля ❌,  якщо помилки немає показати зелену галочку ✅
+ Перевіряти данні на етапі втрати фокуса 
+ та коли йде натискання кнопки відправити дані 

Для себя - дополнительно:
+ когда идёт нажатие кнопки обновить - всё обновляется и формочка очищается

Задача №4:
+ При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. 
+ Це поле буде служити для введення числових значень
Поведінка поля має бути такою:
+ При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. 
+ При втраті фокусу вона пропадає.
+ Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, 
  в якому має бути виведений текст: . 
+ Значення всередині поля введення фарбується зеленим.
+ Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
+ під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
+ Поруч із ним має бути кнопка з хрестиком (`X`).
+ При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
*/

let form = document.querySelector(".form");
let info = document.querySelector("#test");
let podskazka = document.querySelector("#podskazka");

form.addEventListener("change", (el) => {

    // втрата фокусу кожен інпут виводить своє value в параграф з id="test":
    let znachenie = el.target.value;
    //console.log(znachenie);

    el.target.addEventListener("blur", () => {
        info.innerText = `значение ранне-введённого поля: ${znachenie}`;
    })

    // щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів:
    let regulyarka = new RegExp(el.target.dataset.length); // согласно пользовательского атрибута data-length назначенного на элементе 
    console.log(regulyarka);

    // Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною 
    // Якщо поле має помилку показати червоний хрестик біля поля ❌,  якщо помилки немає показати зелену галочку ✅
    if (regulyarka.test(znachenie)) {
        //console.log("true");
        el.target.classList.remove("no");
        el.target.classList.add("yes");
        podskazka.innerText = "";
        el.target.nextElementSibling.innerText = "✅";

    } else {
        //console.log("false");
        el.target.classList.remove("yes");
        el.target.classList.add("no");
        podskazka.innerText = el.target.dataset.podskazka;
        el.target.nextElementSibling.innerText = "❌";
    }

    if (znachenie == "") {
        el.target.classList.remove("yes");
        el.target.classList.add("no");
        podskazka.innerText = "введите, пожалуйста данные";
    }

    // Визначити код оператора та підтягувати логотип оператора:
    if (el.target.type == "tel") { 

        let tel = document.getElementById("tel");

        let sovpadenie = tel.value.search(/^\+380[1-90]{2}/); // после +380 два числа: от 3 до 9 и 0

        let operator = tel.value.substring(4, 6); //вытянуть инфо о двух цифрах, которые идентифицируют оператора

        if (sovpadenie >= 0) { //если совпадение найдено  

            let flag = null;

            function operatorFoto(params) {  // для вывода картинки оператора мобильной связи в div
                document.querySelector(".operator").classList.remove("operator-kievstar");
                document.querySelector(".operator").classList.remove("operator-mts");
                document.querySelector(".operator").classList.remove("operator-life");
                document.querySelector(".operator").classList.add(params);
                return flag = true;
            }

            switch (operator) { //и если есть такой оператор

                case "67":
                case "68":
                case "96":
                case "97":
                case "98":
                case "39":
                    operatorFoto("operator-kievstar");
                    console.log("kievstar. Флаг: " + flag);
                    break;

                case "50":
                case "66":
                case "95":
                case "99":
                    operatorFoto("operator-mts");
                    console.log("mts. Флаг: " + flag);
                    break;

                case "63":
                case "73":
                case "93":
                    operatorFoto("operator-life");
                    console.log("life. Флаг: " + flag);
                    break;

                default: //если не существует такого оператора      
                    flag = false;
            }

            if (flag == false) {
                document.querySelector(".operator").classList.remove("operator-kievstar");
                document.querySelector(".operator").classList.remove("operator-mts");
                document.querySelector(".operator").classList.remove("operator-life");

                console.log("Флаг: " + flag);

                podskazka.innerText = 'не существует такого оператора';  // подсказка 
                el.target.nextElementSibling.innerText = "❌";
                el.target.classList.add("no"); //рамка красная  
            }
        }
    }
})

// Список з містами України:

let spisok = document.querySelector("#spisok-city"); // найти то место куда будем вставлять генерированные теги

// через перебор массива (из json) - вытянуть необх. свойство и сгенерировать теги:
cityes.forEach(el => {
    let option = document.createElement("option");
    option.value = el.city; //фиксированному св-ву созданного элемента назначаем - ключ из объектов массива (из json)
    spisok.append(option);
})

let viborSity = document.querySelector("#city");
//console.dir(viborSity);

viborSity.addEventListener("blur", () => {

    if (viborSity.value == "") {
        document.querySelector("#podskazka").innerText = 'пожалуйста, выберите город';  // подсказка      
    }
})

viborSity.addEventListener("focus", () => {  //при нажатии второй раз, но значение уже есть (передумал)
    if (!viborSity.value == "") {
        viborSity.value = "";
    }
})

// При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. 
// Це поле буде служити для введення числових значень
// так как данный инпут создан отдельно - скорее всего из-за этого он немного сдвинут !!!!!!!!!!!!!!!!!!!!!!!
window.addEventListener("DOMContentLoaded", () => {

    let label = document.createElement("label");
    info.before(label);
    label.setAttribute("for", "chislo");
    label.innerText = "Введите число: ";

    let input = document.createElement("input");
    label.after(input);
    input.setAttribute("type", "text"); // можно было ввести type="number"
    input.setAttribute("placeholder", "Price");
    input.setAttribute("id", "chislo");
    input.dataset.length = "^\\d+$";
    input.dataset.podskazka = `введите число от 0, без пробелов, символов и букв. 
    Please enter correct price`;

    let div = document.createElement("div");
    input.after(div);
    div.classList.add("znachok");

    let br = document.createElement("br");
    div.after(br);

    // При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору.
    input.addEventListener("focus", (el) => {
        el.target.classList.add("yes");
    })

    // При втраті фокусу рамка зеленого кольору пропадає.
    // Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст.
    // Поруч із ним має бути кнопка з хрестиком (`X`)
    // При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
    // Значення всередині поля введення фарбується зеленим.
    // Якщо користувач ввів число менше 0 - `span` зі значенням при цьому не створюється.
    let span = document.createElement("span");
    label.before(span);
    span.after(br);
    span.classList.add("span");
    span.classList.add("displeyNone");

    input.addEventListener("blur", function () {

        this.classList.toggle("yes");

        if (!this.classList.contains("yes")) {  //если нет ошибки (число свыше 0)

            span.innerText = `вы только что ввели число: ${this.value}`;
            span.classList.toggle("displeyNone");

            let div = document.createElement("div");
            span.append(div);
            div.classList.add("znachok");
            div.innerText = "❌";

            div.addEventListener("click", () => {
                div.innerText = "";
                span.classList.toggle("displeyNone");
                return;
            })
            return;
        }
    })

    input.addEventListener("focus", (el) => { //при нажатии второй раз, но значение уже есть (передумал)

        if (!el.target.value == "") {
            el.target.value = "";
            div.innerText = "";
            span.classList.toggle("displeyNone");
        }
    })
})

// Перевіряти данні коли йде натискання кнопки відправити дані

let otpravka = document.querySelector(".otpravka");
let [...inputs] = document.getElementsByTagName("input");
//console.log(otpravka);

otpravka.addEventListener("click", () => {  // при нажатии на кнопку
    form.addEventListener("submit", (event) => { // происходит отправка формочки 

        inputs.forEach((el) => {

            if (el.classList.contains("no")) {  // если есть ошибка (красная рамочка)
                event.preventDefault(); // отменяет отправку
                document.querySelector("#podskazka").innerText = 'пожалуйста, исправьте ошибки';
                console.log("Ошибка");
            }

            if (el.value == "") {  // если есть пустое поле
                event.preventDefault(); // отменяет отправку
                document.querySelector("#podskazka").innerText = 'пожалуйста, заполните пустые поля';
                console.log("пустые поля");
            }
        })
    })
})
//console.log(Array. isArray(inputs));
//console.log(inputs);

// для себя - дополнительно: когда идёт нажатие кнопки обновить - всё обновляется и формочка очищается
let obnovit = document.querySelector(".obnovit");

obnovit.addEventListener("click", () => {  // при нажатии на кнопку
    form.addEventListener("reset", (event) => { // происходит сброс формочки

        inputs.forEach((el) => { // а точнее, очищаются все инпуты
            el.value == "";
            el.classList.remove("no");
            el.classList.remove("yes");
            el.nextElementSibling.innerText = "";
        })

        // и очищается всё остальное
        document.querySelector(".operator").classList.remove("operator-kievstar");
        document.querySelector(".operator").classList.remove("operator-mts");
        document.querySelector(".operator").classList.remove("operator-life");
        podskazka.innerText = "";
        info.innerText = "";

        // отдельный сброс для инпута Price
        document.querySelector(".span").classList.add("displeyNone");
        document.querySelector("#chislo").nextElementSibling.innerText = "";
        document.querySelector("#chislo").classList.remove("no");
        document.querySelector("#chislo").classList.remove("yes");

        console.log("сброс");
        console.dir(inputs);
    })
})