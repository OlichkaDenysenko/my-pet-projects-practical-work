/* Описание:
Необходимо создать CRM
Тема: Авторизация. try и catch. Video. Создание и валидация полей. Сбор данных для хранилища. Рандомное число.
*/

import { find, findAll, look, analisis, authorization, createInpute, validasiya, object, sohrVLocalStorage, tablichkaDannih } from "./functions.js"

/*
Домашняя работа №12:
+ Оформить вёрстку первой страницы
+ Оформить вёрстку авторизации 
+ Оформить логин и пароли и авторизацию через JS (логин и пароль вывести в console.log)
+ Оформить модальное окно
+ Оформление кнопок ("Добавить" и три кнопки по категориям)
+ Оформление полей для ввода данных (на своё усмотрение) и их валидация 
+ Сохранение данных для каждой категории (з категории)
+ Вывести данные в таблицах категорий 

Дополнительно для себя:
+ Оформить кнопки - "Главная" и "Выйти"
+ Сделать возможным удалить / откорректировать данные
*/

/*
# дня |	На украинском |	На русском | На английском |
 0     Неділя          Воскресенье   Sunday
 1	   Понеділок	   Понедельник	 Monday
 2	   Вівторок	       Вторник	     Tuesday
 3	   Середа	       Среда	     Wednesday
 4	   Четвер	       Четверг	     Thursday
 5	   П`ятниця	       Пятница	     Friday
 6	   Субота	       Суббота	     Saturday	
*/

// страница АВТОРИЗАЦИЯ
try {
    authorization();

    // при наведении на все input
    let [...inputs] = findAll(".forma input");
    inputs.forEach((input) => {

        input.addEventListener("focus", () => {
            input.classList.add("focus");
        })
    })

    // логин: день недели на англ. + число
    let daysEngl = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let login = `${daysEngl[new Date().getDay()]}` + `${new Date().getDate() < 10 ? `0${new Date().getDate()}` : `${new Date().getDate()}`}`;
    look(`логин: ` + login.toLowerCase());

    let patternLogin = new RegExp("^" + login.toLowerCase() + "$");
    let loginFlag = null;

    let loginInput = find(".login");
    loginInput.addEventListener("change", (el) => {

        if (patternLogin.test(el.target.value)) {
            el.target.classList.add("ok");
            el.target.classList.remove("error");
            loginFlag = true;
        }

        if (!patternLogin.test(el.target.value)) {
            el.target.classList.add("error");
            el.target.classList.remove("ok");
            loginFlag = false;
        }

        if (el.target.value.length == 0) {
            el.target.classList.add("error");
            el.target.classList.remove("ok");
            el.target.classList.remove("focus");
            loginFlag = false;
        }
    })

    // пароль: день недели на укр. + месяц + год
    let daysUkr = ["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П`ятниця", "Субота"];
    let password =
        `${daysUkr[new Date().getDay()]}` +
        `${new Date().getMonth() + 1 < 10 ? `0${new Date().getMonth() + 1}` : `${new Date().getMonth() + 1}`}` +
        `${new Date().getFullYear()}`;
    look(`пароль: ` + password.toLowerCase());

    let patternPassword = new RegExp("^" + password.toLowerCase() + "$");
    let passwordFlag = null;

    let passwordInput = find(".password");
    passwordInput.addEventListener("change", (el) => {

        if (patternPassword.test(el.target.value)) {
            el.target.classList.add("ok");
            el.target.classList.remove("error");
            passwordFlag = true;
        }

        if (!patternPassword.test(el.target.value)) {
            el.target.classList.add("error");
            el.target.classList.remove("ok");
            passwordFlag = false;
        }

        if (el.target.value.length == 0) {
            el.target.classList.add("error");
            el.target.classList.remove("ok");
            el.target.classList.remove("focus");
            passwordFlag = false;
        }
    })

    // кнопка "Войти" 
    let vhod = find(".vhod");
    vhod.addEventListener("click", () => {

        //сохраняемся и заходим
        if (loginFlag == true && passwordFlag == true) {
            localStorage.login = true;
            document.location = "/";
        }
    })

    //look(patternLogin)

} catch (error) {
    look(`на странице: АВТОРИЗАЦИЯ - ошибка: ` + error);
}

// ГЛАВНАЯ страница:

// Белый блок для добавления
try {

    //кнопка Добавить:
    let add = find(".add");
    let addModalka = find(".add-modalka");

    add.addEventListener("click", () => {
        add.firstElementChild.innerHTML = `&#9997;`; // украшение главной кнопки
        addModalka.classList.add("displayBlock");
    })

    //выбрать категорию, заполнить поля
    let vvod = find("#vvod");

    vvod.addEventListener("change", function () {

        //оформить поля
        if (this.value == undefined) {
            return;
        }

        if (this.value == "restoran") {
            let opisanie = ["Название блюда", "Картинка", "Вес блюда", "Состав", "Стоимость блюда"];
            createInpute(opisanie);
        }

        if (this.value == "magazin") {
            let opisanie = ["Название товара", "Картинка", "Вес товара", "Ключевые слова", "Стоимость товара"];
            createInpute(opisanie);
        }

        if (this.value == "video") {
            let opisanie = ["Название видео", "Адрес хранения", "Жанр", "Страна", "Год", "Рейтинг", "Режиссёр", "Краткое описание", "Ключевые слова"];
            createInpute(opisanie);
        }

        //валидация и редактирование полей
        let [...polya] = findAll(".polya input");
        validasiya(polya)

        //look(polya);
    })

    //Кнопка - закрыть
    let zakr = find(".zakr");

    zakr.addEventListener("click", () => {
        add.firstElementChild.innerHTML = `&#128221;`; // украшение главной кнопки
        addModalka.classList.remove("displayBlock");
    })

    zakr.addEventListener("click", () => {

        let [...polya] = findAll(".polya input");

        //очистить заполненность полей
        polya.forEach((input) => {

            input.value = "";
            input.classList.remove("error");
            input.classList.remove("ok");
        })

        //look(data);
    })

    //Кнопка - сохранить
    let sohran = find(".sohran");

    sohran.addEventListener("click", (e) => {

        let [...polya] = findAll(".polya input");
        let arr = [];

        //заполненность полей
        polya.forEach((input) => {

            if (input.value.length !== 0) {
                arr.push("+");
            }
        })

        //если всё заполнено - сохранить, закрыть и очистить
        if (arr.length == polya.length) {

            //сохранить значения в объект и в localStorage [{},{},{}]
            try {

                if (vvod.value == "restoran") {

                    let [...opisanie] = findAll(".polya label");
                    let [...zapolnenie] = findAll(".polya input");

                    let obj = object(opisanie, zapolnenie); //создать obj для localStorage                

                    sohrVLocalStorage("restoran", obj); //сохранить obj в localStorage  

                }

                if (vvod.value == "magazin") {

                    let [...opisanie] = findAll(".polya label");
                    let [...zapolnenie] = findAll(".polya input");

                    let obj = object(opisanie, zapolnenie); //создать jsonObj для localStorage   

                    sohrVLocalStorage("magazin", obj); //сохранить jsonObj в localStorage      

                }

            } catch (error) {
                look(`при сохранении значения в объекты "restoran"/"magazin" - ошибка: ` + error);
            }

            try {

                if (vvod.value == "video") {

                    let [...opisanie] = findAll(".polya label");

                    let arrZapolnenie = [];

                    opisanie.forEach((el) => {
                        arrZapolnenie.push(el.nextElementSibling);
                    })

                    let obj = object(opisanie, arrZapolnenie); //создать jsonObj для localStorage   

                    sohrVLocalStorage("video", obj); //сохранить jsonObj в localStorage   

                }

            } catch (error) {
                look(`при сохранении значения в объект "video" - ошибка: ` + error);
            }

            add.firstElementChild.innerHTML = `&#128221;`;
            addModalka.classList.remove("displayBlock");

            polya.forEach((input) => {
                input.value = "";
                input.classList.remove("ok");
                input.classList.remove("error");
            })
        }

        if (arr.length !== polya.length) {
            e.preventDefault();
        }

        //look(data);
    })

    //look(sohran);

} catch (error) {
    look(`на ГЛАВНОЙ странице, в белом блоке для добавления - ошибка: ` + error);
}

// Левый блок - вывод КАТЕГОРИИ
try {

    //Ресторан:
    let restoranGlavn = find(".restoran-glavn");

    restoranGlavn.addEventListener("click", () => {
        tablichkaDannih("Ресторан");
    })

    //Магазин:
    let magazinGlavn = find(".magazin-glavn");

    magazinGlavn.addEventListener("click", () => {
        tablichkaDannih("Магазин");
    })

    //Видео:   
    let videoGlavn = find(".video-glavn");

    videoGlavn.addEventListener("click", () => {
        tablichkaDannih("Видео");
    })

    //look(restoranGlavn);

} catch (error) {
    look(`на ГЛАВНОЙ странице, в левом блоке - КАТЕГОРИИ - ошибка: ` + error);
}

// Верхний блок - шапка
try {

    //кнопка Выйти:
    let vihod = find(".vihod");
    vihod.addEventListener("click", () => {        
        localStorage.login = false;
        authorization();
    })

    //look(vihod);

} catch (error) {
    look(`на ГЛАВНОЙ странице, в верхнем блоке-шапки - ошибка: ` + error);
}

