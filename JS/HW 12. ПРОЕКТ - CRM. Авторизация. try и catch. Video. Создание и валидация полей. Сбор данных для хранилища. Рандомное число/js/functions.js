// поиск элемента
const find = (selector) => {
    return document.querySelector(selector);
}

// поиск коллекции
const findAll = (selector) => {
    return document.querySelectorAll(selector);
}

// просмотр элемента
const look = (element) => {
    return console.log(element);
}

// разбор элемента
const analisis = (element) => {
    return console.dir(element);
}

// авторизация
const authorization = () => {

    //если уже находишься на этой странице - выйти
    if (document.location.pathname.search("authorization") !== -1) {
        return;
    }

    //если не зарегистрировался (не сущ. или false) - перевести на страницу регистрации
    if (!localStorage.login) {
        document.location = "/authorization";
    }
}

// создание рамдомного числа с буквами:
function randomChislo() {

    let alfavit = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    let one = Math.floor(Math.random() * alfavit.length);
    let two = Math.floor(Math.random() * alfavit.length);
    let tre = Math.floor(Math.random() * alfavit.length);
    let treBukvi = alfavit[one] + alfavit[two] + alfavit[tre];


    let randomChislo = Math.floor(Math.random() * 1000 + 1);
    let id = randomChislo + treBukvi;

    return id;
}

// созданние полей для заполнения описания товара для опред. категории
function createInpute(arr) {

    let polya = find(".polya");
    polya.innerHTML = "";

    let pole = arr.map((opisanie) => {

        if (opisanie.search("Вес") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
            <div>
              <label for="${id}">${opisanie}</label><input id="${id}" class="ves" type="number">
            </div>
             `
        }
        else if (opisanie.search("Стоимость") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
            <div>
              <label for="${id}">${opisanie}</label><input id="${id}" class="price">
            </div>
             `
        }
        else if (opisanie.search("Год") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
            <div>
              <label for="${id}">${opisanie}</label><input id="${id}" type="month">
            </div>
             `
        }
        else if (opisanie.search("Рейтинг") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
            <div>
            <label for="${id}">${opisanie}</label>
            <select id="${id}" class="reiting">
               <option value="one">★</option>
               <option value="two">★★</option>
               <option value="tre">★★★</option>
               <option value="for">★★★★</option>
               <option value="five">★★★★★</option>               
            </select>
            </div>
             `
        }
        else if (opisanie.search("Жанр") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
            <div>
              <label for="${id}">${opisanie}</label>
              <select id="${id}">
                 <option value="Комедии">Комедии</option>
                 <option value="Мультфильмы">Мультфильмы</option>
                 <option value="Ужасы">Ужасы</option>
                 <option value="Фантастика">Фантастика</option>
                 <option value="Триллеры">Триллеры</option>
                 <option value="Боевики">Боевики</option>
                 <option value="Мелодрамы">Мелодрамы</option>
                 <option value="Детективы">Детективы</option>
              </select>
            </div>
             `
        }
        else {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
            <div>
              <label for="${id}">${opisanie}</label><input id="${id}">
            </div>
            `
        }

    }).join("");

    return polya.insertAdjacentHTML("afterbegin", pole);
}

// созданние полей для редактирования товара
function createInputeForRedact(opisanie) {

    let plus = find(".plus");

    let modalka = `
                <div class="parent-modalka-redact">
                 <div class="modalka-redact">
                 
                  ${opisanie.map((opisanie) => {

        if (opisanie.search("Вес") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
                        <div>
                          <label for="${id}">${opisanie}</label><input id="${id}" class="ves" type="number">
                        </div>
                         `
        }
        else if (opisanie.search("Стоимость") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
                        <div>
                          <label for="${id}">${opisanie}</label><input id="${id}" class="price">
                        </div>
                         `
        }
        else if (opisanie.search("Жанр") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
                                    <div>
                                      <label for="${id}">${opisanie}</label>
                                      <select id="${id}">
                                         <option value="Комедии">Комедии</option>
                                         <option value="Мультфильмы">Мультфильмы</option>
                                         <option value="Ужасы">Ужасы</option>
                                         <option value="Фантастика">Фантастика</option>
                                         <option value="Триллеры">Триллеры</option>
                                         <option value="Боевики">Боевики</option>
                                         <option value="Мелодрамы">Мелодрамы</option>
                                         <option value="Детективы">Детективы</option>
                                      </select>
                                    </div>
                                     `
        }
        else if (opisanie.search("Год") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
                        <div>
                          <label for="${id}">${opisanie}</label><input id="${id}" type="month">
                        </div>
                         `
        }
        else if (opisanie.search("Рейтинг") !== -1) {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
                        <div>
                        <label for="${id}">${opisanie}</label>
                        <select id="${id}" class="reiting">
                           <option value="one">★</option>
                           <option value="two">★★</option>
                           <option value="tre">★★★</option>
                           <option value="for">★★★★</option>
                           <option value="five">★★★★★</option>               
                        </select>
                        </div>
                         `
        }
        else {
            let id = randomChislo();  //обратится к функции создания рамдомного числа
            return `
                         <div>
                           <label for="${id}">${opisanie}</label><input id="${id}">
                         </div>
                         `
        }

    }).join("")
        }
                  <div class="knopki-redact">
                  <button class="sohr-redact">Сохранить</button> 
                  <button class="zakr-redact">Закрыть</button>
                  </div>

                 </div>
                </div>                
                `
    plus.insertAdjacentHTML("afterbegin", modalka);
}

//валидация и редактирование полей
function validasiya(polya) {

    // все поля с разных категорий
    polya.forEach((input) => {

        input.addEventListener("input", () => {

            if (input.value.length == 0) {
                input.classList.add("error");
                input.classList.remove("ok");
            }
            else if (input.value.length > 0) {
                input.classList.add("ok");
                input.classList.remove("error");
            }
        })
    })

    // поля, где есть вес и цена
    try {
        let ves = find(".ves");
        let price = find(".price");

        ves.addEventListener("change", () => {
            if (ves.value < 0) {
                ves.value = 0;
            }
        })

        // в случае, если нажимать на клавиатуре клавиши - не связанные с числами
        price.addEventListener("keypress", (e) => {

            let forNumber = [".", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

            if (!forNumber.includes(e.key)) {
                price.value = `0.00 грн.`;
            }

            //look(e.key);
        })

        price.addEventListener("change", () => {

            if (price.value < 0) {
                price.value = `0.00 грн.`;
            }
            else {
                price.value = `${parseFloat(price.value).toFixed(2)} грн.`;
            }

            //look(typeof price.value)
        })

    } catch (error) {
        look(`на ГЛАВНОЙ странице, в белом блоке для добавления, валидация и редактирование полей (вес/цена) - ошибка: ` + error);
    }
}

//создание объекта (для localStorage)
function object(arrOpisanie, arrZapolnenie) {

    let newArrOpisanie = [];
    let newArrZapolnenie = [];

    arrOpisanie.forEach((pole) => {
        newArrOpisanie.push(pole.innerText);
    })

    arrZapolnenie.forEach((pole) => {
        newArrZapolnenie.push(pole.value);
    })

    let obj = {};

    for (let a = 0; a < arrOpisanie.length; a++) {

        obj[newArrOpisanie[a]] = newArrZapolnenie[a];
    }

    //дата создания объекта чч.мм.гг
    let data = `${new Date().getDate() < 10 ? `0${new Date().getDate()}` : `${new Date().getDate()}`}` + `${new Date().getMonth() + 1 < 10 ? `.0${new Date().getMonth() + 1}` : `.${new Date().getMonth() + 1}`}` + `.${new Date().getFullYear()}`;

    obj.data = data;

    return obj;
}

// сохранить в localStorage [{},{},{}]
function sohrVLocalStorage(nameLocalStorage, obj) {

    if (!localStorage[nameLocalStorage]) { //если не сущ. - создать и вложить массив

        let arr = []; // создать массив - хранилище для объектов
        let json = JSON.stringify(arr); // перевести в json и сохранить localStorage  
        localStorage[nameLocalStorage] = json;
    }

    if (localStorage[nameLocalStorage]) { //если сущ. - заполнить   

        let arr = JSON.parse(localStorage[nameLocalStorage]); //вытянуть массив из json
        arr.push(obj); //добавить объект
        let json = JSON.stringify(arr); // перевести в json и сохранить localStorage  
        localStorage[nameLocalStorage] = json;
    }
}

//создать табличку с данными категорий
function tablichkaDannih(kategoriya) {

    //очистить
    let block = find(".plus");
    block.innerHTML = "";

    //отобразить
    if (kategoriya == "Ресторан") {

        let arr = JSON.parse(localStorage.restoran); //вытянуть массив с объектами из json 

        let tablichka = `
                <table class="tablichka">
                    <thead>
                        <tr>
                            <th>№:</th>
                            <th>Название:</th>
                            <th>Картинка:</th>
                            <th>Цена:</th>
                            <th>Остаток:</th>
                            <th>Дата добавления:</th>                            
                            <th>Отредактировать</th>
                            <th>Удалить</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${arr.map((obj, index) => {
            return `
                                <tr>
                                    <td>${index + 1}</td>
                                    <td>${obj["Название блюда"]}</td>
                                    <td><img class="kartinka" src="${obj["Картинка"]}" alt=${obj["Название блюда"]} title=${obj["Название блюда"]}</td>
                                    <td>${obj["Стоимость блюда"]}</td>
                                    <td>${Math.floor(Math.random() * 100)} порций</td>
                                    <td>${obj.data}</td>                                    
                                    <td class="redact" data-index="${index}">📝</td>
                                    <td class="udalit" data-index="${index}">❌</td>
                                </tr>`
        }).join("")
            }                        
                    </tbody>
                </table>`

        block.insertAdjacentHTML("afterbegin", tablichka);

        //отредактировать        
        let [...redact] = findAll(".redact");

        redact.forEach((kart) => {

            kart.addEventListener("click", () => {

                //вытянуть из массива определ. объект
                let indexTarget = kart.dataset.index;
                let objTarget = arr[indexTarget];

                // удалить свойство - "дата создания"
                if ("data" in objTarget) {
                    delete objTarget.data;
                }

                //составить заново поля
                let opisanie = ["Название блюда", "Картинка", "Вес блюда", "Состав", "Стоимость блюда"];
                createInputeForRedact(opisanie);

                //отобразить модальное окно
                let parentModalkaRedact = find(".parent-modalka-redact");
                parentModalkaRedact.classList.add("displayFlex");

                //заполнить поля данными
                let [...inputs] = findAll(".modalka-redact input");
                let arrValues = Object.values(objTarget);

                for (let a = 0; a < inputs.length; a++) {
                    inputs[a].value = arrValues[a];
                }

                //валидация и редактирование полей
                validasiya(inputs);

                //Кнопка - сохранить              
                let sohrRedact = find(".sohr-redact");

                sohrRedact.addEventListener("click", (e) => {

                    let arrValue = [];

                    //заполненность полей
                    inputs.forEach((input) => {

                        if (input.value.length !== 0) {
                            arrValue.push("+");
                        }
                    })

                    if (arrValue.length !== inputs.length) {
                        e.preventDefault();
                    }

                    //если всё заполнено - ПЕРЕсохранить
                    if (arrValue.length == inputs.length) {

                        let [...opisanie] = findAll(".modalka-redact label");
                        let [...zapolnenie] = findAll(".modalka-redact input");

                        let obj = object(opisanie, zapolnenie); //создать obj для localStorage               

                        arr[indexTarget] = obj;

                        let json = JSON.stringify(arr); // перевести в json и сохранить localStorage  
                        localStorage.restoran = json;

                        //обновить страницу, закрыть(удалить модульное окно):
                        tablichkaDannih("Ресторан");

                        parentModalkaRedact.remove();

                        // look(arr[indexTarget])
                        // look(arr)
                    }

                    //look(arrValue)
                })

                //Кнопка - закрыть

                let zakrRedact = find(".zakr-redact");

                zakrRedact.addEventListener("click", () => {
                    parentModalkaRedact.remove();
                })
                //look(arrValue)
            })
        });

        //удалить
        let [...udalit] = findAll(".udalit");

        udalit.forEach((kart) => {

            kart.addEventListener("click", () => {

                //вытянуть из массива определ. объект и удалить
                let ind = kart.dataset.index;

                arr.splice(ind, 1);

                let json = JSON.stringify(arr); // перевести в json и сохранить localStorage  
                localStorage.restoran = json;

                //обновить страницу:
                tablichkaDannih("Ресторан");

                //look(arr)
            })
        })
    }

    if (kategoriya == "Магазин") {

        let arr = JSON.parse(localStorage.magazin); //вытянуть массив с объектами из json 

        let tablichka = `
                <table class="tablichka">
                    <thead>
                        <tr>
                            <th>№:</th>
                            <th>Название:</th>
                            <th>Картинка:</th>
                            <th>Цена:</th>
                            <th>Остаток:</th>
                            <th>Дата добавления:</th>                            
                            <th>Отредактировать</th>
                            <th>Удалить</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${arr.map((obj, index) => {
            return `
                                <tr>
                                    <td>${index + 1}</td>
                                    <td>${obj["Название товара"]}</td>
                                    <td><img class="kartinka" src="${obj["Картинка"]}" alt=${obj["Название блюда"]} title=${obj["Название блюда"]}</td>
                                    <td>${obj["Стоимость товара"]}</td>
                                    <td>${Math.floor(Math.random() * 100)} шт.</td>
                                    <td>${obj.data}</td>                                    
                                    <td class="redact" data-index="${index}">📝</td>
                                    <td class="udalit" data-index="${index}">❌</td>
                                </tr>`
        }).join("")}    
                    </tbody>
                </table>`

        block.insertAdjacentHTML("afterbegin", tablichka);

        //отредактировать        
        let [...redact] = findAll(".redact");

        redact.forEach((kart) => {

            kart.addEventListener("click", () => {

                //вытянуть из массива определ. объект
                let indexTarget = kart.dataset.index;
                let objTarget = arr[indexTarget];

                // удалить свойство - "дата создания"
                if ("data" in objTarget) {
                    delete objTarget.data;
                }

                //составить заново поля
                let opisanie = ["Название товара", "Картинка", "Вес товара", "Ключевые слова", "Стоимость товара"];
                createInputeForRedact(opisanie);

                //отобразить модальное окно
                let parentModalkaRedact = find(".parent-modalka-redact");
                parentModalkaRedact.classList.add("displayFlex");

                //заполнить поля данными
                let [...inputs] = findAll(".modalka-redact input");
                let arrValues = Object.values(objTarget);

                for (let a = 0; a < inputs.length; a++) {
                    inputs[a].value = arrValues[a];
                }

                //валидация и редактирование полей
                validasiya(inputs);

                //Кнопка - сохранить              
                let sohrRedact = find(".sohr-redact");

                sohrRedact.addEventListener("click", (e) => {

                    let arrValue = [];

                    //заполненность полей
                    inputs.forEach((input) => {

                        if (input.value.length !== 0) {
                            arrValue.push("+");
                        }
                    })

                    if (arrValue.length !== inputs.length) {
                        e.preventDefault();
                    }

                    //если всё заполнено - ПЕРЕсохранить
                    if (arrValue.length == inputs.length) {

                        let [...opisanie] = findAll(".modalka-redact label");
                        let [...zapolnenie] = findAll(".modalka-redact input");

                        let obj = object(opisanie, zapolnenie); //создать obj для localStorage               

                        arr[indexTarget] = obj;

                        let json = JSON.stringify(arr); // перевести в json и сохранить localStorage  
                        localStorage.magazin = json;

                        //обновить страницу, закрыть(удалить модульное окно):
                        tablichkaDannih("Магазин");

                        parentModalkaRedact.remove();

                        // look(arr[indexTarget])
                        // look(arr)
                    }

                    //look(arrValue)
                })

                //Кнопка - закрыть

                let zakrRedact = find(".zakr-redact");

                zakrRedact.addEventListener("click", () => {
                    parentModalkaRedact.remove();
                })
                //look(arrValue)
            })
        });

        //удалить
        let [...udalit] = findAll(".udalit");

        udalit.forEach((kart) => {

            kart.addEventListener("click", () => {

                //вытянуть из массива определ. объект и удалить
                let ind = kart.dataset.index;

                arr.splice(ind, 1);

                let json = JSON.stringify(arr); // перевести в json и сохранить localStorage  
                localStorage.magazin = json;

                //обновить страницу:
                tablichkaDannih("Магазин");

                //look(arr)
            })
        })
    }

    if (kategoriya == "Видео") {

        let arr = JSON.parse(localStorage.video); //вытянуть массив с объектами из json 

        let tablichka = `
                <table class="tablichka">
                    <thead>
                        <tr>
                            <th>№:</th>
                            <th>Название:</th>
                            <th>Воспроизведение:</th>                           
                            <th>Дата публикации:</th>                            
                            <th>Отредактировать</th>
                            <th>Удалить</th>
                        </tr> 
                    </thead>
                    <tbody>
                        ${arr.map((obj, index) => {
            return `
                                <tr>
                                    <td>${index + 1}</td>
                                    <td>${obj["Название видео"]}</td>
                                    <td><video class="kartinka" src="${obj["Адрес хранения"]}" controls></video></td>                                    
                                    <td>${obj.data}</td>                                    
                                    <td class="redact" data-index="${index}">📝</td>
                                    <td class="udalit" data-index="${index}">❌</td>
                                </tr>`
        }).join("")} 
                    </tbody>
                </table>`

        block.insertAdjacentHTML("afterbegin", tablichka);

        //отредактировать        
        let [...redact] = findAll(".redact");

        redact.forEach((kart) => {

            kart.addEventListener("click", () => {

                //вытянуть из массива определ. объект
                let indexTarget = kart.dataset.index;
                let objTarget = arr[indexTarget]; // предыдущие значения

                // console.log(objTarget);

                // удалить свойства: "дата создания" - чтобы обновить  
                if ("data" in objTarget) {
                    delete objTarget.data;
                }

                // удалить свойства: "Жанр" и "Рейтинг" - т.к. это не инпуты
                if ("Жанр" in objTarget) {
                    delete objTarget.Жанр;
                }
                if ("Рейтинг" in objTarget) {
                    delete objTarget.Рейтинг;
                }

                //составить заново поля
                let opisanie = ["Название видео", "Адрес хранения", "Жанр", "Страна", "Год", "Рейтинг", "Режиссёр", "Краткое описание", "Ключевые слова"];
                createInputeForRedact(opisanie);

                //отобразить модальное окно
                let parentModalkaRedact = find(".parent-modalka-redact");
                parentModalkaRedact.classList.add("displayFlex");

                //заполнить поля данными
                let [...inputs] = findAll(".modalka-redact input"); // выбрать все поля input
                let arrValues = Object.values(objTarget); // предыдущие значения

                for (let a = 0; a < inputs.length; a++) {
                    inputs[a].value = arrValues[a];
                }

                //валидация и редактирование полей
                validasiya(inputs);

                //Кнопка - сохранить              
                let sohrRedact = find(".sohr-redact");

                sohrRedact.addEventListener("click", (e) => {

                    let arrValue = [];

                    //заполненность полей
                    inputs.forEach((input) => {

                        if (input.value.length !== 0) {
                            arrValue.push("+");
                        }
                    })

                    if (arrValue.length !== inputs.length) {
                        e.preventDefault();
                    }

                    //если всё заполнено - ПЕРЕсохранить
                    if (arrValue.length == inputs.length) {

                        let [...opisanie] = findAll(".modalka-redact label");

                        let arrZapolnenie = [];

                        opisanie.forEach((el) => {
                            arrZapolnenie.push(el.nextElementSibling);
                        })

                        let obj = object(opisanie, arrZapolnenie); //создать obj для localStorage               

                        arr[indexTarget] = obj;

                        let json = JSON.stringify(arr); // перевести в json и сохранить localStorage  
                        localStorage.video = json;

                        //обновить страницу, закрыть(удалить модульное окно):
                        tablichkaDannih("Видео");

                        parentModalkaRedact.remove();

                        // look(arr[indexTarget])
                        // look(arr)
                    }

                    //look(arrValue)
                })

                //Кнопка - закрыть

                let zakrRedact = find(".zakr-redact");

                zakrRedact.addEventListener("click", () => {
                    parentModalkaRedact.remove();
                })
                //look(arrValue)
            })
        });

        //удалить
        let [...udalit] = findAll(".udalit");

        udalit.forEach((kart) => {

            kart.addEventListener("click", () => {

                //вытянуть из массива определ. объект и удалить
                let ind = kart.dataset.index;

                arr.splice(ind, 1);

                let json = JSON.stringify(arr); // перевести в json и сохранить localStorage  
                localStorage.video = json;

                //обновить страницу:
                tablichkaDannih("Видео");

                //look(arr)
            })
        })
    }
}

export { find, findAll, look, analisis, authorization, createInpute, validasiya, object, sohrVLocalStorage, tablichkaDannih };

