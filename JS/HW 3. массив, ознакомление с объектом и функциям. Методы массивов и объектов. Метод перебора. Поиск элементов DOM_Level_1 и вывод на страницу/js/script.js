/* Описание:
Необходимо решить задачи.
Тема: массив, ознакомление с объектом и функциям. Методы массивов и объектов. Метод перебора. Поиск элементов DOM_Level_1 и вывод на страницу*/

/*Задача №1:
Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата". 
Створити об'єкт вкладений об'єкт - "Додаток". 
Створити об'єкт "Додаток", вкладені об'єкти, "Заголовок, тіло, футер, дата". 
Створити методи для заповнення та відображення документа.*/

let document_ = {

    zagolovok: "паспорт",
    tilo: "ЕЕ 525252",
    footer: "орган: 5252",
    data: "выдан: 02.02.2002г",

    dodatok: {
        zagolovok: "ИНН",
        tilo: "525252525252",
        footer: "орган: 2525",
        data: "выдан: 05.05.2005г"
    }
};

document.write("<br>");
document.write(`высвечивается додаток:<br>`);
document.write("<br>");

for (let doc in document_.dodatok) {
    document.write(`${document_.dodatok[doc]}<br>`);
}

document.write("<br>");
document.write(`высвечивается документ:<br>`);
document.write("<br>");

for (let doc in document_) {

    if ("dodatok" in document_) {
        delete document_.dodatok;
    }
    document.write(`${document_[doc]} <br>`);
}

document.write("<br>");

/* Задача №2:
 Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, 
 та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.
 Двумя действиями:
*/

document.write(`действие №1:
создала функцію та масив, яка обробляє кожен елемент масиву цією функцією <br>`);
document.write("<br>");

let cc = [1, 2, 3];
//document.write(Array.isArray(с));

function d(a) {
    document.write(`обработанный елемент массива: ${a} <br>`);
}

cc.forEach(d);

document.write("<br>");

document.write(`действие №2:
Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, повертаючи новий масив. <br>`);
document.write("<br>");

function map(fn, array) {
    return fn = [4, 5, 6];
}

let rez_ = map(d, cc);
//document.write(Array.isArray(rez_));
document.write(`новый массив: rez = [ ${rez_} ]`);

document.write("<br>");
document.write("<br>");

document.write(`Задача №2:
Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.
Одним действием: <br>`);

document.write("<br>");

function dd() { }

let c = [1, 2, 3];
//document.write(Array.isArray(с));

function map(fn, array) {

    array.forEach(fn);
    document.write(`обработанный елемент массива: ${array} <br>`);

    return array = [4, 5, 6];
}

document.write("<br>");

let rez = map(dd, c);
//document.write(Array.isArray(rez));

document.write(`новый массив: rez = [ ${rez} ]`);
document.write("<br>");


/* Задача №3:
Створіть сторінку коментарів та стилізуйте її як сторінка відгуків. 
За допомогою методів перебору виведіть на сторінку коментарі, пошту, номер коментаря та автора
Дані тут : https://jsonplaceholder.typicode.com/comments
*/

//document.write(Array.isArray(info));

info.forEach(function (a, b) {

    let blok = `
     <div class="shapka">
      <div>Номер комментария:</div>            
      <div class="nomer">${b + 1}</div>
     </div> 

     <div class="telo">
      <div class="avtor">${a.name}</div> 
      <div class="pochta">${a.email}</div>
      <div class="otziv">${a.body}</div>
     </div>
    `
    document.getElementById("konteiner").innerHTML += blok;
})

