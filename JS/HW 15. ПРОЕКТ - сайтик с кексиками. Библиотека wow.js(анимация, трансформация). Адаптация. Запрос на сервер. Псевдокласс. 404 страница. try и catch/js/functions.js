//оформление карточек через сервер
async function zapros(url) {
    const otvet = await fetch(url);
    return otvet.json();
}

//проявить миникорзину
function shoyMiniKorzinki() {

    let itogKolvo = document.querySelector(".itog-kolvo");

    if (!localStorage.miniKorzinka) { //если не существует корзинки - миникорзинка обнуляется

        localStorage.miniKorzinka = 0;
        itogKolvo.innerText = localStorage.miniKorzinka;

    } else {

        itogKolvo.innerText = localStorage.miniKorzinka;
    }
}
shoyMiniKorzinki();

//процедура выбора кол-ва кексика
function plusMinus(pervZnachenie, znak) {

    let kolvoItogo;

    if (znak == "+") {
        kolvoItogo = pervZnachenie + 1;
    }

    if (znak == "-") {

        if (pervZnachenie <= 0) {

            kolvoItogo = 0;

        } else {
            kolvoItogo = pervZnachenie - 1;
        }
    }

    return kolvoItogo;
}

//оформление корзинки с записью на localStorage
function korzina(obj) {

    if (!localStorage.korzina) {
        let arr = [];

        let json = JSON.stringify(arr);
        localStorage.korzina = json;

        shoyMiniKorzinki();
    }

    if (localStorage.korzina) {
        let arr = JSON.parse(localStorage.korzina);

        //нужно оформить замену 
        let newArr = arr.filter((el) => {  //удаляем такой же объект, выводим новый массив без него
            return el.artikul !== obj.artikul;
        });

        if (obj.kolvo !== 0) { //если кол-во не 0 - добавляем нужный объект, если 0 - не сохраняем
            newArr.push(obj);
        }

        if (newArr.length != 0) {

            let json = JSON.stringify(newArr);
            localStorage.korzina = json;

        } else {

            localStorage.clear();
            localStorage.registr = "уже посетил страницу"; 
        }       

        //сохраняем и выводим общее кол-во в миникорзину
        let miniKorzinkaArr = [];

        newArr.forEach((element) => {
            miniKorzinkaArr.push(element.kolvo);
        });

        let miniKorzinka = miniKorzinkaArr.reduce((a, b) => {
            return a += b;
        }, 0);

        localStorage.miniKorzinka = miniKorzinka;
        shoyMiniKorzinki();
    }
}

//отобразить мини-список в мини-корзинке
function lookMiniSpisok() {

    let spisok = document.querySelector(".spisok");
    spisok.innerHTML = "";

    if (localStorage.korzina) {
        let arrKorzina = JSON.parse(localStorage.korzina);

        let arrItog = arrKorzina.map((obj) => {

            let itog = (obj.kolvo * obj.price).toFixed(2);

            let card = `
            <div class="card-spisok">
                <div class="card-spisok-info">
                    <div class="card-spisok-img"><img src="/${obj.img}" alt="cake"></div>
                    <div class="card-spisok-name">${obj.name}</div>
                    <div class="card-spisok-kol">x ${obj.kolvo}</div>
                </div>
                <div class="card-itog">$${itog}</div>
            </div>
            `
            spisok.innerHTML += card;

            return Number(itog);
        })

        let itogo = arrItog.reduce((a, b) => {
            return a += b;
        }, 0).toFixed(2);

        return itogo;

    } else {

        spisok.innerHTML = `
        <img src="/img/7qwZ.gif" alt="В корзине пусто" title="в ожидании вашего заказа" width="100%">
        `
        return "0.00";
    }

}

export { zapros, korzina, plusMinus, lookMiniSpisok };