/* Описание:
Необходимо создать проект, согласно макета из Фигмы.
Тема: библиотека wow.js(анимация, трансформация). Адаптация. Запрос на сервер. Псевдокласс. 404 страница. try и catch.
*/

import { zapros, korzina, plusMinus, lookMiniSpisok } from "./functions.js";

/*
 Домашняя работа №15.
Создать сайт, согласно : https://www.figma.com/file/amCRbZADhX1ni8wVPKHeRX/HW-JS?type=design&node-id=1-2&mode=design&t=BbEmRq2r9zG4F9OS-0

+ Сверстать сайт: HTML + CSS
+ контентная часть всех страниц в 4 адаптациях:
  ПК от 1367 - 1200px, Ноутбуки от 1024 до 1366 - 992px, Планшеты от 768 до 1023 - 768px и Мобильные до 767 - 320px.
+ оформить информационное сообщение, словно после регистрации
+ три кексика на первой странице должны быть анимированные - выезжать из трёх сторон
+ самостоятельно ознакомиться и разобраться с библиотекой wow.js
+ кексик на первой странице из двух частей должен быть разобран и при скроле собираться в единое целое, с помощью библиотеки: wow.js
+ при нажатии кнопки shop now - происходит авто скрол вниз до выбора кексиков
+ создание карточек товара через JSON
с помощью JS: 
+ вывод карточек с помощью запроса на сервер
+ при выборе кол-ва кексика - количество отображается, но пока нигде не сохраняется
+ создание мини-корзиночки вверху страницы. 
+ c помощью JS: отображается общее количество кексиков. 
+ с помощью JS:  при нажатии на кнопку ADD TO CART: 
(через LocalStorage, чтобы данные не слетели в случае перезагрузки страницы.)
  + сохраняется в корзине: выбор кексика и его количество, 
  + сохранятся и отображаются: кол-во кексика в карточке товара, 
  + сохраняются и отображаются: общее кол-во кексиков в мини-корзиночке  
+ В случае если в кол-ве кексика проставлен 0 - удалять кексик из корзины и из списка мини-корзины.
+ с помощью JS:  в случае отсутствия корзины (пустой корзины) 
  + обнулить данные в мини-корзиночке и 
  + все количества во всех кексиках
+ создать список выбранных кексиков
+ при нажатии мини-корзиночки отображается список выбранных кексиков, 
+ стоимость за количество одного вида кексика, 
+ общая цена за все кексики 
  и кнопки: 
+ 1 - при нажатии которой выведет на первую станицу и опустится в секцию выбора кексиков, 
+ 2 - при нажатии которой выведет на страницу - корзину.
+ последний кексик на первой странице анимировать
+ в подвале страницы - оформить корректность ввода электронной почты, сохранить почту в LocalStorage.
+ создание третьей страницы - показать из чего состоит кексик: все части кексика анимированные
+ c помощью псевдокласса оформить заголовок картинками
+ создание четвёртой страницы - корзинка покупок
+ при оформлении покупки (формирование заказа) - удалить корзину, обнулить мини-корзину, обнулить все количества во всех кексиках.
+ в случае пустой корзины (или при её отсутствии) - проявлять картинку, которая отображает ожидание заказа
+ так как второй страницы - не было в задании - оформить “404 страницу”
*/

//                     ГЛАВНАЯ СТРАНИЦА

// закрыть инфо. Если уже раз заходил - значит проходил регистрацию - значит нет необх. повторять информацион. сообщение
try {

  let info = document.querySelector(".info");
  let zakrInfo = document.querySelector(".zakr-info");

  if (localStorage.registr) {
    info.classList.add("displayNone");
  }

  zakrInfo.addEventListener("click", () => {
    info.classList.add("displayNone");
    localStorage.registr = "уже посетил страницу";
  })

} catch {
  console.log("oшибка в блоке: ГЛАВНАЯ СТРАНИЦА");
}

// вывод карточек
try {

  zapros("/js/cards.json")
    .then((result) => {

      try {

        result.forEach((obj) => {

          let cards = document.querySelector(".cards");

          let card = `
          <div class="card" data-artikul="${obj.artikul}" data-price="${obj.price}">
           
            <div class="img-cake"><img src="${obj.img}" alt="cupcake" title="${obj.name}"></div>
            <div class="name-cake">${obj.name}</div>
            <div class="text-cake">${obj.text}</div>
           
           <div class="vibor">
           
             <div class="sumka">
                <div class="minus">-</div>
                <div class="kol">0</div>
                <div class="plus">+</div>
              </div>
   
              <button class="add">ADD TO CART</button>
           </div>     
          </div>  
          `
          cards.innerHTML += card;
        });

      } catch {
        console.log("oшибка в блоке: ГЛАВНАЯ СТРАНИЦА при выводе карточек");
      }

      //высветить количества каждого кексика сохранённое из localStorage, в случае перезагрузки страницы
      let [...kols] = document.querySelectorAll(".kol");

      kols.forEach((kolichestvo) => {

        let card = kolichestvo.closest(".card"); //найти текущую карточку и её данные (артикул)
        let artikul = card.dataset.artikul;

        if (!localStorage[artikul]) { //если выбор ещё не был сделан

          kolichestvo.innerText = 0;

        } else {

          kolichestvo.innerText = localStorage[artikul];
        }

        //если не существует корзинки - по всем кексикам обновляются количества
        if (!localStorage.korzina) {
          kolichestvo.innerText = 0;
        }
      })

      //работа с корзинкой    

      let itogSpiska = document.querySelector(".itog-spiska");
      itogSpiska.innerText = "$00.00";

      //добавить товар
      let [...plusiki] = document.querySelectorAll(".plus");
      plusiki.forEach((plus) => {

        plus.addEventListener("click", () => {

          let kol = plus.previousElementSibling; //найти элемент количества 
          let kolValue = parseInt(kol.innerText); //найти его значение

          kol.innerText = plusMinus(kolValue, "+");
        })
      })

      //отнять товар
      let [...minusiki] = document.querySelectorAll(".minus");
      minusiki.forEach((minus) => {

        minus.addEventListener("click", () => {

          let kol = minus.nextElementSibling; //найти элемент количества 
          let kolValue = parseInt(kol.innerText); //найти его значение

          kol.innerText = plusMinus(kolValue, "-");
        })
      })

      //добавить в корзину
      let [...adds] = document.querySelectorAll(".add");
      adds.forEach((button) => {

        button.addEventListener("click", () => {

          let card = button.closest(".card"); //найти текущую карточку и её данные (артикул и кол-во)
          let artikul = card.dataset.artikul;

          let arr = result.filter((el) => {
            return el.artikul == artikul;
          })

          let obj = arr[0];
          obj.kolvo = parseInt(button.previousElementSibling.innerText[2]);

          korzina(obj);

          //сохранить кол-во кексика, на случае перезагрузки страницы
          localStorage[artikul] = obj.kolvo;

          //закрыть мини-список корзинки
          spisokKorzinki.classList.remove("displayBlock");
        })
      })

    })

} catch {
  console.log("oшибка в блоке: ГЛАВНАЯ СТРАНИЦА");
}

//                        СТРАНИЦА КОРЗИНКИ
try {

  //если корзинки не существует - отобразить губку Боба и изменить инфо в кнопке оформления покупки  
  let gybka = document.querySelector(".gybka");
  let marg = document.querySelector(".marg");

  if (!localStorage.korzina) {

    gybka.classList.add("displayBlock");
    gybka.classList.remove("displayNone");

  } else {
    gybka.classList.remove("displayBlock");
    gybka.classList.add("displayNone");
  }

  //кнопка оформления покупки  
  marg.addEventListener("click", (e) => {

    if (!localStorage.korzina) {

      alert("Пожалуйста, сделайте заказ!");
      e.preventDefault();

    } else {

      alert("Спасибо за заказ. Ваша оплата прошла успешно!");
      localStorage.clear();
      localStorage.registr = "уже посетил страницу";
    }

  })

} catch {
  console.log("oшибка в блоке: СТРАНИЦА КОРЗИНКИ");
}

try {

  let arr = JSON.parse(localStorage.korzina);
  let cards = document.querySelector(".section-two-page-korz");

  //итог списка
  let itogo = document.querySelector(".itog-spiska-page-korz");

  //отображение корзины
  let arrItog = arr.map((obj) => {

    let itog = (obj.kolvo * obj.price).toFixed(2);

    let card = `
    <div class="card-korzinki" data-artikul="${obj.artikul}">

       <div class="img-card-korzinki">
           <img src="/${obj.img}" alt="cake" title="${obj.name}">
       </div>

       <div class="name-card-korzinki">${obj.name}</div>

       <div class="sumka-card-korzinki">
           <div class="minus-card-korzinki">-</div>
           <div class="kol-card-korzinki">${obj.kolvo}</div>
           <div class="plus-card-korzinki">+</div>
       </div>

       <div class="itog-card-korzinki">$${itog}</div>

    </div>
    `
    cards.innerHTML += card;

    return Number(itog);
  })

  //первоначальный итог списка
  itogo.innerHTML = `$${arrItog.reduce((a, b) => {
    return a += b;
  }, 0).toFixed(2)}`;

  //добавить - отобразить - отправить в корзину        
  let [...plusCardKorzinki] = document.querySelectorAll(".plus-card-korzinki");
  plusCardKorzinki.forEach((plus) => {

    plus.addEventListener("click", () => {

      let kol = plus.previousElementSibling; //найти элемент количества 
      let kolValue = parseInt(kol.innerText); //найти его значение

      kol.innerText = plusMinus(kolValue, "+");

      //найти текущую карточку и её данные (артикул и кол-во)
      let cardKorzinki = plus.closest(".card-korzinki");
      let artikul = cardKorzinki.dataset.artikul;

      let arrt = arr.filter((el) => {
        return el.artikul == artikul;
      })

      let object = arrt[0];
      object.kolvo = parseInt(kol.innerText);

      //нужно оформить замену 
      korzina(object);

      //сохранить кол-во кексика, на случае перезагрузки страницы
      localStorage[artikul] = object.kolvo;

      //пересчитать итог кексика
      let sumkaCardKorzinki = plus.closest(".sumka-card-korzinki");
      let itogi = sumkaCardKorzinki.nextElementSibling;
      let itog = (object.kolvo * object.price).toFixed(2);
      itogi.innerText = `$${itog}`;

      //пересчитать итог списка
      let arrItog = arr.map((obj) => {

        let itog = (obj.kolvo * obj.price).toFixed(2);

        return Number(itog);
      })

      itogo.innerHTML = `$${arrItog.reduce((a, b) => {
        return a += b;
      }, 0).toFixed(2)}`;


    })
  })

  //отнять - отобразить - отправить в корзину
  let [...minusCardKorzinki] = document.querySelectorAll(".minus-card-korzinki");
  minusCardKorzinki.forEach((minus) => {

    minus.addEventListener("click", () => {

      let kol = minus.nextElementSibling; //найти элемент количества 
      let kolValue = parseInt(kol.innerText); //найти его значение

      kol.innerText = plusMinus(kolValue, "-");

      //найти текущую карточку и её данные (артикул и кол-во)
      let cardKorzinki = minus.closest(".card-korzinki");
      let artikul = cardKorzinki.dataset.artikul;

      let arrt = arr.filter((el) => {
        return el.artikul == artikul;
      })

      let object = arrt[0];
      object.kolvo = parseInt(kol.innerText);

      //нужно оформить замену 
      korzina(object);

      //сохранить кол-во кексика, на случае перезагрузки страницы
      localStorage[artikul] = object.kolvo;

      //пересчитать итог кексика
      let sumkaCardKorzinki = minus.closest(".sumka-card-korzinki");
      let itogi = sumkaCardKorzinki.nextElementSibling;
      let itog = (object.kolvo * object.price).toFixed(2);
      itogi.innerText = `$${itog}`;

      //пересчитать итог списка
      let arrItog = arr.map((obj) => {

        let itog = (obj.kolvo * obj.price).toFixed(2);

        return Number(itog);
      })

      let dd = arrItog.reduce((a, b) => {
        return a += b;
      }, 0).toFixed(2);

      itogo.innerHTML = `$${dd}`;

      if (dd == "0.00") {
        localStorage.clear();
        localStorage.registr = "уже посетил страницу";        
      }

    })
  })

} catch {
  console.log("oшибка в блоке: СТРАНИЦА КОРЗИНКИ");
}

//                         HEADER

//burger
let burger = document.querySelector(".burger");
let menu = document.querySelector(".menu");

burger.addEventListener("click", () => {
  menu.style.display = "block";
  burger.style.display = "none";
})

//отобразить и закрыть мини-список корзинки
let miniKorzinka = document.querySelector(".mini-korzinka");
let spisokKorzinki = document.querySelector(".spisok-korzinki");

let x = document.querySelector(".x");

let proverka = document.querySelector(".proverka");
let vKorzinky = document.querySelector(".v-korzinky");

let itogSpiska = document.querySelector(".itog-spiska");
itogSpiska.innerText = "$0.00";

miniKorzinka.addEventListener("click", () => {

  spisokKorzinki.classList.add("displayBlock");

  let itogo = lookMiniSpisok();
  itogSpiska.innerText = `$${itogo}`;
})

x.addEventListener("click", () => {
  spisokKorzinki.classList.remove("displayBlock");
})

proverka.addEventListener("click", () => {
  spisokKorzinki.classList.remove("displayBlock");
})

vKorzinky.addEventListener("click", () => {
  spisokKorzinki.classList.remove("displayBlock");
})

//                       FOOTER

//высветить текущую дату в подвале страницы
let data = document.querySelector(".data");
data.innerText = `©MISS CUPCAKES 2020 - ${new Date().getFullYear()}`;

//оформить корректность ввода электронной почты, сохранить почту в LocalStorage в подвале страницы
let email = document.querySelector(".email");
let send = document.querySelector(".send");
let flag = null;

email.addEventListener("blur", () => {

  let reg = /^[A-z0-9.-]+@[a-z]+\.[a-z]{2,4}$/;

  if (reg.test(email.value)) {
    email.classList.remove("error");
    flag = true;

  } else {
    flag = false;
    email.classList.add("error");
  }
})

send.addEventListener("click", (e) => {

  if (flag == true) {
    localStorage.email = email.value;
    email.value = "";

  } else {
    e.preventDefault();
  }
})


