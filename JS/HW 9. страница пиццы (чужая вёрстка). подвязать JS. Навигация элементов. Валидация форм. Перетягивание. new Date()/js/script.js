/* Описание:
Необходимо доделать сайт пиццерии. Подвязать JS к элементам 
Тема: Валидация форм. Создание прайса и выбор покупателя. Перетягивание. new Date()
*/

//Домашняя работа №9. 1 часть

const date = `© 2002 – ${new Date().getFullYear()} . Мережа піцерій Domino!`;
document.getElementById("address").innerText = date;

// проверка инпутов и сохранение пользователя
let validate = (r, v) => {
    return r.test(v);
};

const user = {
    name: "",
    phone: "",
    email: ""
};

const [...inputsForm] = document.querySelectorAll("#contact-form input");

function inputValidate() {

    //debugger  - остановка для наладчика    

    if (this.type === "text" && validate(/^[А-яіїґєA-z-]+$/i, this.value)) {
        user.name = this.value; // .toLowerCase()
        this.classList.add("success");
        this.classList.remove("error");
    } else if (this.type === "email" && validate(/^[A-z_0-9.]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/, this.value)) {
        user.email = this.value; // .toLowerCase()
        this.classList.add("success");
        this.classList.remove("error");
    } else if (this.type === "tel" && validate(/^\+380[0-9]{9}$/, this.value)) {
        user.phone = this.value;
        this.classList.add("success");
        this.classList.remove("error");
    }
    else {
        this.classList.add("error");
        this.classList.remove("success");
    }
}

inputsForm.forEach((el) => {

    if (el.type === "text" || el.type === "email" || el.type === "tel") {
        el.addEventListener("change", inputValidate);
    }
})

// кнопка "Підтвердити замовлення >>" и проверка формы
const form = document.getElementById("contact-form");
let zakaz = document.getElementById("btnSubmit");

zakaz.addEventListener("click", () => {

    let flag = [];

    inputsForm.forEach((el) => {

        if (el.type === "text" || el.type === "email" || el.type === "tel") {

            if (el.classList.contains("error")) {
                return
            }
            else if (el.value == "") {
                return
            }
            else {
                flag.push("+");
            }
        }
    })

    if (flag.length == 3) {
        document.location = "/final";
    } else {
        console.log("oшибка заполнения полей");
    }
})

// кнопка "Зкинути"
let skinyt = document.getElementById("cancel");
skinyt.addEventListener("click", () => {

    inputsForm.forEach((el) => {

        if (el.type === "text" || el.type === "email" || el.type === "tel") {

            el.classList.remove("error");
            el.classList.remove("success");
        }
    })
})

// создание прайса
class Price {

    constructor() {

        this.size = [{ name: "small", price: 50 }, { name: "mid", price: 75 }, { name: "big", price: 100 }];
        this.sizePrice = null; //вывод цены - при выборе size

        this.sous = [{ name: "Кетчуп", price: 10 }, { name: "BBQ", price: 10 }, { name: "Рiкотта", price: 10 }];
        this.sousesPrice = null; //вывод цены - при выборе соусов

        this.toping = [{ name: "Сир звичайний", price: 15 }, { name: "Сир фета", price: 15 }, { name: "Моцарелла", price: 15 }, { name: "Телятина", price: 20 }, { name: "Помiдори", price: 10 }, { name: "Гриби", price: 15 }];
        this.topingsPrice = null; //вывод цены - при выборе топингов

        this.kolichestvo = null;
    }

    calc() {

        return (this.sizePrice + this.sousesPrice + this.topingsPrice) * this.kolichestvo;
    }

}

let newZakaz = new Price(); // обращение к прайсу

//первоначальный выбор
let itog = document.querySelector(".itog");

let priceShow = document.querySelector(".pri"); // вывести инфо размера
priceShow.innerText = 50;
newZakaz.sizePrice = 50;

let kolichestvoInput = document.querySelector(".input_"); // вывести количествo
kolichestvoInput.value = 1;
newZakaz.kolichestvo = 1;

itog.innerText = newZakaz.calc();

// выбор размера пиццы:
let sizeChoice = document.querySelector("#pizza");

sizeChoice.addEventListener("click", (el) => {

    if (!el.target.closest("span")) {  //если нажал не на <span> - выйти и продолжить
        return;
    }

    let input = el.target.closest("label").firstElementChild;  // добраться до <input> чтобы забрать value
    //console.log(input.value); 

    //перебрать массив size и записать значения
    for (let obj of newZakaz.size) {

        let nameSize = obj.name; //значение name - в виде строки
        let priceSize = obj.price;  //значение price - в виде числа
        //console.log(typeof priceSize);

        if (nameSize.includes(input.value)) { // найти совпадение значения имени из <input> и из массива  
            newZakaz.sizePrice = priceSize;  // записать в прайсе - вывод цены size   

            priceShow.innerText = priceSize;
            itog.innerText = newZakaz.calc();
        }
    }

    console.log(newZakaz.sizePrice);
    //console.log(el.target); // проверить на какой элемент нажато
})

// выбор количества:
kolichestvoInput.addEventListener("change", () => {

    if (kolichestvoInput.value < 1) { // менее 1
        kolichestvoInput.value = 1;
    }
    if (kolichestvoInput.value > 10) { // более 10
        kolichestvoInput.value = 10;
    }

    newZakaz.kolichestvo = kolichestvoInput.value;  // записать в прайсе - количество пицц   
    itog.innerText = newZakaz.calc();
})

// "перетягивание начинок":

// начинки
let [...draggable] = document.querySelectorAll(".draggable");

draggable.forEach((img) => {

    img.addEventListener("dragstart", function (el) {
        el.dataTransfer.setData("text", this.src);
    })
})

// выбор соусов:
let [...sousChoice] = document.querySelectorAll(".sous");

let sumSous = []; // для сложения кoличества выбранных соусов

for (let img of sousChoice) { //перебрать массив картинок соусов

    img.addEventListener("dragend", (el) => { // на каждую картинку повесить событие

        if (!el.target.closest("img")) {  //если нажал не на картинку - выйти и продолжить
            return;
        }

        let span = el.target.nextElementSibling;  // добраться до <span> с инфо

        //перебрать массив sous и записать значения
        for (let obj of newZakaz.sous) {

            let nameSous = obj.name; //значение name - в виде строки
            let priceSous = obj.price;  //значение price - в виде числа
            //console.log(typeof priceSize);

            if (nameSous.includes(span.innerText)) { // найти совпадение значения имени из <span> и из массива  

                sumSous.push(priceSous);  // добавить в массив если выбрать несколько соусов
                let sumirovanie = sumSous.reduce((akk, el) => { return akk + el }) //суммирование елементов массива

                newZakaz.sousesPrice = sumirovanie;  // записать в прайсе - вывод суммы всех соусов 

                let priceShow = document.querySelector(".sau"); // вывести инфо
                priceShow.innerText = sumirovanie;
                itog.innerText = newZakaz.calc();
            }
        }
        //console.log(el.target); // проверить на какой элемент нажато
    })
}

// выбор топингов:
let [...topingChoice] = document.querySelectorAll(".toping");

let sumToping = []; // для сложения кoличества выбранных топингов

for (let img of topingChoice) { // перебрать массив картинок топингов

    img.addEventListener("dragend", (el) => { // на каждую картинку повесить событие

        if (!el.target.closest("img")) {  //если нажал не на картинку - выйти и продолжить
            return;
        }

        let span = el.target.nextElementSibling;  // добраться до <span> с инфо

        //перебрать массив toping и записать значения
        for (let obj of newZakaz.toping) {

            let nameToping = obj.name; //значение name - в виде строки
            let priceToping = obj.price;  //значение price - в виде числа
            //console.log(typeof priceToping);

            if (nameToping.includes(span.innerText)) { // найти совпадение значения имени из <span> и из массива  

                sumToping.push(priceToping);  // добавить в массив если выбрать несколько топингов
                let sumirovanie = sumToping.reduce((akk, el) => { return akk + el }) //суммирование елементов массива

                newZakaz.topingsPrice = sumirovanie;  // записать в прайсе - вывод суммы всех топингов 

                let priceShow = document.querySelector(".top"); // вывести инфо
                priceShow.innerText = sumirovanie;
                itog.innerText = newZakaz.calc();
            }
        }
        // console.log(el.target); // проверить на какой элемент нажато
    })
}

// целевой элемент "пицца"
let pizza = document.querySelector(".table");

pizza.addEventListener("dragover", (e) => {
    if (e.preventDefault) {
        e.preventDefault();
    }
    return false;
})

pizza.addEventListener("drop", function (e) {
    if (e.preventDefault) {
        e.preventDefault();
    }
    if (e.stopPropagation) {
        e.stopPropagation();
    }

    let src = e.dataTransfer.getData("Text");
    let kartinka = document.createElement("img");
    kartinka.src = src;
    this.appendChild(kartinka);

    return false;
})

// console.log(draggable);







