/* Описание:
Необходимо решить задачки
Тема: конструктор ES6. Навигация элементов. События элементов DOM_Level_2
*/

//Домашняя работа №9. 2 часть

// 1. Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода имени и фамилии

class Person {

    constructor(name, familiya) {
        this.name = name;
        this.familiya = familiya;
    }
    vivesti() {
        console.log(`Здраствуйте, ${this.name} ${this.familiya}!`)
    }
}

let newObject = new Person("Оля", "Денисенко");
newObject.vivesti();

// 2. Создай список состоящий из 4 листов. Используя джс обратись ко 2 li и с использованием навигации по DOM дай 1 элементу синий фон, а 3 красный

let liTwo = document.getElementsByTagName("li")[1];
liTwo.previousElementSibling.classList.add("blue");
liTwo.nextElementSibling.classList.add("red");
//console.log(liTwo);

// 3. Создай див высотой 400 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстом координаты, где находится курсор мышки

let oneDiv = document.querySelector(".one-div");

oneDiv.addEventListener("mouseover", (el) => {
    oneDiv.innerText = 
    ` координаты, где находится курсор мышки: 
    по оси X: ${el.clientX}
    по оси Y: ${el.clientY}`;
    //console.log("наведение мышки");
})
//console.log(oneDiv);

// 4. Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата

let [...buttons] = document.getElementsByTagName("button");
//console.log(buttons);

buttons.forEach((el) => {
    el.addEventListener("click", () => {
        oneDiv.innerText = `${el.value}`;
        //console.dir(el);
    })
})

// 5. Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице

let twoDiv = document.querySelector(".two-div");
//console.log(twoDiv);

twoDiv.addEventListener("mouseover", (el) => {

    twoDiv.style.bottom = el.clientY + 10 + "px";
    twoDiv.style.right = el.clientX + 10 + "px";

    let y = parseInt(twoDiv.style.bottom);
    let x = parseInt(twoDiv.style.right);

    if (y > document.body.clientHeight) {
        twoDiv.style.bottom = Math.floor(Math.random() * document.body.clientHeight + 1) + "px";
    }
    if (x > document.body.clientWidth) {
        twoDiv.style.right = Math.floor(Math.random() * document.body.clientWidth + 1) + "px";
    }

    twoDiv.classList.remove("two-div-green");
    twoDiv.innerText = `Нравится ли Вам Ваша зарплата? Если нет - нажмите на этот блок`;

    //console.log("<div> находится на высоте: " + twoDiv.style.bottom);
    //console.log("<div> находится по горизонтали: " + twoDiv.style.right);
    //console.log("наведение мышки");    
})

twoDiv.addEventListener("click", (el) => {

    twoDiv.style.bottom = 5 + "px";
    twoDiv.style.right = 5 + "px";
    twoDiv.classList.add("two-div-green");
    twoDiv.innerText = `Не переживайте, всё у вас будет хорошо!`;
})

// 6. Создай поле для ввода цвета, когда пользователь выберет какой-то цвет - сделай его фоном body

let inputColor = document.getElementsByTagName("input")[0];

inputColor.addEventListener("change", () => {
    document.body.style.backgroundColor = inputColor.value;
})

// 7. Создай инпут для ввода логина, когда пользователь начнет вводить данные в инпут - выводи их в консоль

let inputText = document.getElementsByTagName("input")[1];

inputText.addEventListener("input", () => {
    console.log(inputText.value);
})

// 8. Создайте поле для ввода данных. После введения данных - выведите текст под полем

let p = document.getElementsByTagName("p")[0];

inputText.addEventListener("change", () => {
    p.innerText = inputText.value;
})
