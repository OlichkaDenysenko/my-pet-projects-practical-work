/* Описание:
Необходимо решить задачки.
Тема: условия и циклы. Ознакомление с массивами. */

//задачка №1:

let a1 = prompt("введите первое число");
let b1 = prompt("введите второе число");

let aa = parseFloat(a1);
let ba = parseFloat(b1);

let result = (aa + ba < 4) ? "мало (до 4)" : "много (более 4)";
document.write(result + "<br/>" + "<br/>");

//задачка №2: 

let login = prompt("Введите логин");
let massage = (login == "Вася") ? "Привет" : (login == "Директор") ? "Здравствуйте" : (login == null) ? "Нет логина" : "Введите правильный логин";
alert(massage);


//задачка №3:

// 1 Действие:
// вывести в виде таблицы: 

let aaa = 0;
let baa = 10;
let resultat_ = [];

while (aaa < baa) {
  resultat_ += (`${aaa} + 1 = ${(aaa + 1)} <br/>`);
  aaa++;
}
document.write(`сумма чисел в виде таблицы = <br/> ${resultat_} <br/>`);

// просто общая сумма всех чисел:

let ac = 0;
let bc = 10;
let resultat = 0;
for (let i = ac + 1; i <= bc; i++) {
  resultat += i;
}
document.write(`сумма чисел от А до В: 0 + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 = ${resultat} <br/> <br/>`);

// 2 Действие:

let av = 0;
let bv = 10;

for (let i = av + 1; i <= bv; i++) {

  if (i % 2 == 1) {
    document.write(`Нечётное число =  ${i}; <br/> <br/>`);
  }
}


//задачка №4:

/*
Використовуючи цикли намалюйте звёздочками: 
1 Заповнений прямокутник
2 Порожній прямокутник
3 Заповнений Рівносторонній трикутник 
4 Заповнений Прямокутний трикутник 
5 Заповнений Ромб
*/

//1 Заповнений прямокутник:

for (let risunok = 0; risunok < 11; risunok++) {

  for (let risunok2 = 0; risunok2 < 11; risunok2++) {
    document.write("*");
  }

  document.write("<br>");
}
document.write("<br>");

//2 Порожній прямокутник:

for (let a = 0; a <= 10; a++) {

  for (let b = 0; b <= 10; b++) {
    if (a >= 1 && b >= 1 && a <= 9 && b <= 9) {
      document.write("&nbsp;&nbsp;");
    } else {
      document.write("*");
    }
  }
  document.write("<br>");
}
document.write("<br>");

//3 Заповнений Рівносторонній трикутник:

for (let i = 0; i <= 10; i++) {

  for (let a = (10 - i); a > 0; a--) {
    document.write("&nbsp;");
  }
  for (let j = 0; j <= i; j++) {
    document.write("*");
  }
  document.write("<br>");
}
document.write("<br>");

//4 Заповнений Прямокутний трикутник:

for (let x = 1; x <= 11; x++) {

  for (let v = (11 - x); v < 11; v++) {
    document.write("*");
  }
  document.write("<br>");
}
document.write("<br>");

//5 Заповнений Ромб:

for (let i = 0; i < 10; i++) {
  for (let a = (11 - i); a > 0; a--) {
    document.write("&nbsp;");
  }
  for (let j = 0; j <= i; j++) {
    document.write("*");
  }
  document.write("<br>");
}

for (let v = 0; v < 11; v++) {
  for (let m = 0; m <= v; m++) {
    document.write("&nbsp;");
  }
  for (let z = (11 - v); z > 0; z--) {
    document.write("*");
  }
  document.write("<br>");
}
document.write("<br>");

//задачка №5:

/*
Створіть масив styles з елементами «Джаз» та «Блюз». 
1) Додайте «Рок-н-рол» до кінця.
2) Замініть значення всередині на «Класика». Ваш код для пошуку значення 
всередині повинен працювати для масивів з будь-якою довжиною
3) Видаліть перший елемент масиву та покажіть його.
4) Вставте «Реп» та «Реггі» на початок масиву.
*/

let styles = ["Джаз", "Блюз"];

let a = styles.push("Рок-н-рол");
document.write(`Первое действие: ${styles}<br><br>`);

styles.splice(styles.length / 2, 1, "Класика");
document.write(`Второе действие: ${styles}<br><br>`);

let b = styles.splice(0, 1);
document.write(`Третье действие: ${b}<br><br>`);

document.write(`перед четвёртым действием: ${styles}<br>`);
document.write(`<br>`);

styles.unshift("Реп", "Реггі");
document.write(`Четвёртое действие: ${styles}`);
