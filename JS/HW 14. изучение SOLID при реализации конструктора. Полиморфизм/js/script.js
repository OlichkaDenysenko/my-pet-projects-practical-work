/* Описание:
Необходимо: 
1) реалізувати клас User на JavaScript, який буде представляти користувача вашого веб-додатка.
2) Підготуйте коротку звітність про те, що ви дізналися від цього відео: https://youtu.be/xq13wiqvcTc 
Тема: изучение SOLID при реализации конструктора. Полиморфизм
*/

// Домашняя работа №14.
/*
Задание: №1

+
Клас має мати наступні поля: 
id - унікальний ідентифікатор користувача; 
name - ім'я користувача; 
email - email користувача; 
password - пароль користувача. 

+
Клас повинен мати наступні методи: 
getId() - повертає id користувача; 
getName() - повертає ім'я користувача; 
getEmail() - повертає email користувача; 
setEmail(email) - встановлює email користувача; 
setPassword(password) - встановлює пароль користувача. 
*/

class User {
    constructor(id, name, email, password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    getId() { //повертає id користувача
        return this.id;
    }

    getName() { //повертає ім'я користувача
        return this.name;
    }

    setEmail(email) { //встановлює email користувача
        return this.email = email;
    }

    getEmail() { //повертає email користувача
        return this.email;
    }

    setPassword(password) { //встановлює пароль користувача
        return this.password = password;
    }

}

let user = new User(5, "Оля", "bobobo@ukr.net", 777777);

// console.log(user);
// console.log(user.getId());
// console.log(user.getName());
// console.log(user.setEmail("lalala@ukr.net"));
// console.log(user.setPassword(8888888));
// console.log(user.getEmail());

/* Задание: №2.

1 принцип - говорит о том, чтобы не использовать полиморфизм.
Например, 
чтобы один отвечал за один основной функционал, 
и создать второй конструктор - который будет отвечать только за способы отображения
*/

class UserPrinter {

    constructor(obj) {
        this.object = obj;
    }

    html() {
        return `        
        <div>iD пользователя:${this.object.id}</div>
        <div>Имя пользователя:${this.object.name}</div>
        <div>E-mail пользователя:${this.object.email}</div>      
        `
    }
}

let printer = new UserPrinter(user); //передаём экземпляр выше прописанного класса, с основным функционалом.
document.body.innerHTML = printer.html();

// если совместить два высше прописанных класса - это будет ПОЛИМОРФИЗМ

/*
2 принцип - говорит о том, чтобы при внесении чего либо нового, не менять свой старый код.
Например, воспользоваться наследием
*/

class Job extends User {  // станет дочерним конструктором

    constructor(id, name, job) {

        super(id, name); //чтобы вызвать родительский конструктор с его параметрами

        this.job = job; // добавляем новый параметр
    }

    work() {
        document.body.append(`${this.name} работает ${this.job}. `);
    }
}

let masha = new Job(44, "Мария", "продавцом в магазине");
masha.work();
masha.setEmail("mamamama@ukr.net"); // обращается к методу из родительского класса, от которого наследовался
//console.log(masha);

/*
3 принцип - суть похожа на предыдущий принцип, 
единственное что одноимённый метод кoторый будет связывать все конструкторы - необходимо вынести в отдельную функцию
*/

class Rabochie extends Job {
    constructor(id, name, job) {
        super(id, name, job);
    }

    dopusk(){
        document.body.append(` ${this.name} не имеет доступ к корпоративному сайту, так как является ${this.job}. `);
    }    
}

class Admin extends Job {
    constructor(id, name, job) {
        super(id, name, job);
    }

    dopusk(){
        document.body.append(` ${this.name} имеет доступ к корпоративному сайту, так как является ${this.job}. `);
    }    
}

function dopuskForPersonal (instans){
    instans.dopusk();
}

let dasha = new Rabochie(158, "Даша", "уборщицей");
let glasha = new Admin(5578, "Глаша", "заместителем директора");
let вasha = new Rabochie(14, "Бавария", "продавцом");
let sasha = new Admin(55, "Александр", "администратором");

dopuskForPersonal(dasha);
dopuskForPersonal(glasha);
dopuskForPersonal(вasha);
dopuskForPersonal(sasha);

/*
4 принцип - суть в том, чтобы избежать лишних элементов поведения.
Например, воспользоваться модификацией с помощью Object.assign()
*/

let admin = {
    obyazannosti(){
        document.body.append(`В обязаности ${this.name}, входит: отчётность и бесперебойная работа магазина. `);
    }
}

let rabochiy = {
    obyazannosti(){
        document.body.append(`В обязаности ${this.name}, входит: соблюдение порядка и чистоты в торговом зале. `);
    }
}

Object.assign(Admin.prototype, admin); // объеденяем два объекта
Object.assign(Rabochie.prototype, rabochiy); // объеденяем два объекта

// например, теперь конструктор Admin будет иметь общий метод: dopusk() и новый: obyazannosti()

let kasha = new Rabochie(158, "Кафелия", "уборщицей");
dopuskForPersonal(kasha); // обращение к основному общему методу
kasha.obyazannosti(); // обращение к новосозданному методу

/*
5 принцип - суть в том, чтобы инвертировать зависимости
*/

//например вместо:
let stepasha = new Admin(578, "Степан", "директором");
dopuskForPersonal(stepasha);

//прописывать сразу:
dopuskForPersonal(new Admin(578, "Степан", "директором"));

