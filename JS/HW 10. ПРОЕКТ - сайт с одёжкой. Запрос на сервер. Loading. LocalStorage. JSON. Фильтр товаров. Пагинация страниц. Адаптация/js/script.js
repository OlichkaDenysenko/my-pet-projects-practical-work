/* Описание:
Необходимо создать сайт с мужской одёжкой, согласно макета из Фигмы
Тема: Запрос на сервер. Loading. LocalStorage. JSON. Фильтр товаров. Пагинация страниц. Адаптация
*/

//Домашняя работа №10.
/*
+ Зверстати сайт який складається з 3х сторінок
+ Виконання роботи (HTML5 & CSS3 + JS + normalize + SCSS - необов'язково)
+ Верстка має бути реалізована через HTML & CSS (Семантична, гумова або статична, валідна, адаптивна до 1023px и вище).

+ Навігаційне меню має бути клікабельним та адаптивним. Оформить бургер - меню.

+ Товари створити на свій розсуд та зберігати у JS з подальшою публікацією на сторінці (JSON). 
 Кожен товар це окремий об'єкт зі своїм набором правил (кольори, розміри і т.д) 
 (товари можна знайти на різних сайтах продажу одягу, товарів має бути не менше 50-шт)

Головна сторінка:
+ На сторінках є товари у вигляді карточок (4 шт)
+ При натиску кнопки ADD TO CART відкриває нову сторінку / модальне віконце з детальним описом товару 
+ (також дані записати в localstorage)

Друга сторінка (усі карточки):
+ На сторінках є товари у вигляді карточок
+ При натиску кнопки ADD TO CART відкриває нову сторінку / модальне віконце з детальним описом товару 
+ (також дані записати в localstorage)
+ використовуючи JS реалізуйте можливість фільтрації товарів (за кольором, за розміром)
+ Перехід між каталогом товарів реалізується пагінацією
+ Товарів має бути по 10-шт на одну сторінку

Третя сторінка (або у вигляді модального окна) (карточка товару):
+ зробити сторінку товару динамічною на свій розсуд
+ реалізувати додавання товару за фільтрами (за кольором та за розміром)
+ При натиску кнопки ADD TO CART додавати товар до кошика (стилізація та реалізація кошика залишається на ваш вибір) 
+ реалізувати коментарі та рейтинги товару

Сделала дополнительно для себя:
+ Логотип возвращает на главную страницу
+ оформила мини корзину сверху в header (высвечивает итог по количеству позиций), при нажатии открывает основную корзину
+ решила выводить карточки словно через запрос на сервер, при этом
+ оформила Loading

На второй странице:
+ с помощью JS реализовала поиск товара, если ввести точное название или даже его часть (только для одного, который находится на второй странице)
+ создала "хлебные крошки"

На третьей странице:
+ сделала вывод нескольких фотографий для просмотра
+ подвязала ссылки для иконок соц.сетей
+ оформила стрелочку "выйти из карточки товара"
+ при реализации комментариев - создала пагинацию

Корзинка:
+ по мимо названия - вывела картинку, выбранный цвет и размер
+ оформила кол-во одной позиции
+ оформила возможность удалить выбранный товар
+ оформила возможность закрыть корзинку
+ сделала "ИТОГ"
+ создала кнопку "продолжить покупки", которая возвращает на прежнюю страницу

Будет время:
- увеличить кол-во адаптаций (хотя бы минимум 4 размеров)
*/


function poisk(selector) {
    return document.querySelector(selector);
}

// Дата
poisk(".address").innerText = `©${new Date().getFullYear()} Sportif Mailorder. All Rights Reserved.`;

//бургер меню:
let burger = poisk(".burger");
let burgerMenu = poisk(".burger-menu");
let korzinaItigo = poisk(".korzina-itigo");

burger.addEventListener("click", () => {
    burger.classList.add("displayNone");
    burgerMenu.classList.add("displayBlock");
    korzinaItigo.classList.add("korzina-itigo-activ");
})

//вывод 4 карточек на 1 странице
let cardsFore = poisk(".section-two-imgs");

let u = "../js/shorts.json";
let xx = fetch(u);

let loadings = poisk(".loading");
loadings.classList.add("displayBlock");

xx
    .then((otvet) => {
        loadings.classList.remove("displayBlock");
        return otvet.json();
    })
    .then((arr) => {

        //для того чтобы выводить разные карточки
        let a = Math.floor(Math.random() * 45);
        let b = a + 4;

        arr.slice(a, b).forEach((obj) => {

            //перебрать массив с цветом
            let divColor = obj.colors.map((color) => {
                let div = document.createElement("div");
                div.classList.add("variant-color");
                div.style.backgroundColor = color;
                return div.outerHTML;
            })

            //вывести карточку
            let kartochka = `
                    <div class="img-rasprod">
                                <div class="artik">${obj.artikul}</div>
                                <div class="block-img-kataloga">
                                    <img class="img-kataloga" src="${obj.photo[0]}" alt="short">
                                </div>
        
                                <div class="name-kartochki-kataloga">${obj.name}</div>
        
                                <div class="zvezds">
                                    <div class="zvezda_one">&#9734;</div>
                                    <div class="zvezda_two">&#9734;</div>
                                    <div class="zvezda_tre">&#9734;</div>
                                    <div class="zvezda_for">&#9734;</div>
                                    <div class="zvezda_five">&#9734;</div>
                                </div>
        
                                <div class="czena">As low as <span class="pricess">$</span><span
                                        class="price pricess">${(obj.price).toFixed(2)}</span></div>
        
                                <div class="variants-color">
                                ${divColor.join("")}
                                </div>
                                
                                <button class="v-korzinky">&#128722; ADD TO BAG</button>
        
                            </div>
                    `;
            cardsFore.insertAdjacentHTML("afterbegin", kartochka);
        })

        // РАБОТА С БОЛЬШОЙ КАРТОЧКОЙ: 

        let [...vKorzinky] = document.querySelectorAll(".v-korzinky");
        let produkt = poisk(".produkt"); // большая карточка 

        vKorzinky.forEach((button) => {

            button.addEventListener("click", () => {

                produkt.classList.add("displayBlock");  //открыть большую карточку

                //найти родителя карточки, где была нажата кнопка корзинки
                let roditel = button.parentElement;

                //добраться до артикула
                let arttik = roditel.firstElementChild.innerText;

                //найти и вывести данную карточку среди массива
                let poiskObj = undefined;

                arr.forEach((obj) => {

                    if (obj.artikul == arttik) {
                        poiskObj = obj;
                    }

                })

                //вывести данные из найденной карточки в большую карточку
                console.log(poiskObj)

                // добавить фото
                let photoOne = poisk(".photo-one");
                photoOne.setAttribute("src", poiskObj.photo[0]);

                let photoTwo = poisk(".card-imgs-two");
                photoTwo.setAttribute("src", poiskObj.photo[1]);

                let photoTre = poisk(".card-imgs-tre");
                photoTre.setAttribute("src", poiskObj.photo[2]);

                //активировать выбранное фото:
                let twoCards = poisk(".two-cards");
                twoCards.addEventListener("click", (el) => {

                    if (!el.target.closest("img")) {
                        return
                    }

                    let [...img] = twoCards.children;

                    img.forEach((element) => {
                        element.classList.remove("activ-img");
                    });

                    el.target.classList.add("activ-img");

                    poisk(".card-imgs-one").firstElementChild.src = el.target.src;  //заменить фото
                })

                // добавить артикул 
                let artikulKod = poisk(".artikul-kod");
                artikulKod.innerText = poiskObj.artikul;

                // добавить цвета 
                let colors = poisk(".colors");

                //перебрать массив со всеми цветами и вывести в элементе
                let divColor = poiskObj.colors.map((color) => {
                    let div = document.createElement("div");
                    div.classList.add("color");
                    div.style.backgroundColor = color;
                    return div.outerHTML;
                })

                colors.innerHTML = divColor.join("");

                // добавить имя
                let nameProdukt = poisk(".name_produkt");
                nameProdukt.innerText = poiskObj.name;

                // добавить цену 
                let price = poisk(".prices .price");
                price.innerText = poiskObj.price.toFixed(2);

                // добавить размеры 
                let sizes = poisk(".sizes");

                //перебрать массив со всеми размерами и вывести в элементе
                let divSize = poiskObj.sizes.map((size) => {
                    let div = document.createElement("div");
                    div.classList.add("size");
                    div.innerText = size;
                    return div.outerHTML;
                })

                sizes.innerHTML = divSize.join("");

                // добавить отзовы
                let otzovs = poisk(".otzovs");
                let newblock = [];

                //перебрать массив с отзывами и вывести в элементе
                let divOtzov = poiskObj.otzovi.map((otzov) => {

                    let div = document.createElement("div");
                    div.classList.add("otzov_");
                    div.innerText = otzov;

                    let otzovZvezda = document.createElement("div");
                    otzovZvezda.classList.add("otzov-zvezda");

                    let zvezda = document.createElement("div");
                    zvezda.classList.add("zvezda");
                    zvezda.innerHTML = `&#9734;&#9734;&#9734;&#9734;&#9734;`

                    otzovZvezda.insertAdjacentHTML("afterbegin", div.outerHTML);
                    otzovZvezda.insertAdjacentHTML("afterbegin", zvezda.outerHTML);

                    return newblock.push(otzovZvezda.outerHTML);
                })

                //вывести отзывы
                let newblockOne = newblock.slice(0, 8); //первые 8 шт
                let newblockTwo = newblock.slice(8);  //остальные

                otzovs.innerHTML = newblockOne.join("");

                // пагинация страниц
                let paginaciyaOne = poisk(".paginaciya-one");
                let paginaciyaTwo = poisk(".paginaciya-two");

                //вначале удалить предыдущие
                paginaciyaOne.addEventListener("click", () => {

                    let [...otzovi] = document.querySelectorAll(".otzov-zvezda");

                    otzovi.forEach((el) => {
                        el.remove();
                    })
                })

                paginaciyaTwo.addEventListener("click", () => {

                    let [...otzovi] = document.querySelectorAll(".otzov-zvezda");

                    otzovi.forEach((el) => {
                        el.remove();
                    })
                })

                //затем добавить необходимые
                paginaciyaOne.addEventListener("click", () => {

                    paginaciyaOne.classList.add("paginaciya-activ");
                    paginaciyaOne.classList.remove("paginaciya-passiv");

                    paginaciyaTwo.classList.add("paginaciya-passiv");
                    paginaciyaTwo.classList.remove("paginaciya-activ");

                    otzovs.insertAdjacentHTML("afterbegin", newblockOne.join(""));

                })

                paginaciyaTwo.addEventListener("click", () => {

                    paginaciyaOne.classList.add("paginaciya-passiv");
                    paginaciyaOne.classList.remove("paginaciya-activ");

                    paginaciyaTwo.classList.add("paginaciya-activ");
                    paginaciyaTwo.classList.remove("paginaciya-passiv");

                    otzovs.insertAdjacentHTML("afterbegin", newblockTwo.join(""));

                })

                //добавить количество отзывов 
                let otzovi = poisk(".otzovi");
                otzovi.innerText = divOtzov.length;

                //РАБОТА С КОРЗИНКОЙ                   

                //объект корзинка для localStorage:
                let object = {
                    photo: undefined,
                    name: undefined,
                    color: undefined,
                    size: undefined,
                    price: undefined,
                }

                // выбор размера:
                sizes.children[0].classList.add("activ");

                sizes.addEventListener("click", (el) => {

                    if (!el.target.closest(".size")) {
                        return
                    }

                    let [...size] = sizes.children;

                    size.forEach((element) => {
                        element.classList.remove("activ");
                    });

                    el.target.classList.add("activ");
                })

                // выбор цвета:
                colors.children[0].classList.add("color-activ");

                colors.addEventListener("click", (el) => {

                    if (!el.target.closest(".color")) {
                        return
                    }

                    let [...color] = colors.children;

                    color.forEach((element) => {
                        element.classList.remove("color-activ");
                    });

                    el.target.classList.add("color-activ");
                })

                // сохранить данные в объект 
                let korzina = poisk(".korzina");

                korzina.addEventListener("click", () => {

                    let nameProdukt = poisk(".name_produkt");
                    object.name = nameProdukt.innerText;

                    let cardImgsOne = poisk(".photo-one");
                    object.photo = cardImgsOne.src;

                    let price = poisk(".prices .price");
                    object.price = price.innerText;

                    let sizeActiv = poisk(".activ");
                    object.size = sizeActiv.innerText;

                    let colorActiv = poisk(".color-activ");
                    let cvet = getComputedStyle(colorActiv)["backgroundColor"];
                    object.color = cvet;

                    //console.log(object); //вывести объект для localStorage
                })

                //из объекта переобразовать и сохранить в localStorage
                korzina.addEventListener("click", () => {
                    localStorage.tovar = JSON.stringify(object);
                })

                //вывести корзинку уже с данными из localStorage
                korzina.addEventListener("click", () => {

                    //вывести  из localStorage                            
                    let ob = JSON.parse(localStorage.tovar);
                    //console.log(ob);

                    let kolichestvo = 1;

                    let tovar = `
                                    <div class="tovar">

                                    <div class="tovar-photo">
                                        <div class="block-photo">
                                            <img class="photo" src= ${ob.photo} alt="short">
                                        </div>
                                    </div>

                                    <div class="tovar-name">
                                        <div>Название: <span>${ob.name}</span></div>

                                        <div class="vibor">

                                            <div class="vibor">Цвет:&nbsp;&nbsp;                
                                                <div class="color" style="background-color:${ob.color};"></div> 
                                            </div>

                                            <div class="vibor">Размер:&nbsp;&nbsp; 
                                                <div class="size">${ob.size}</div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="tovar-itog">

                                        <div class="tovar-itog-kolichestvo">Kол-во:

                                            <div class="kolichestvo">
                                                <div class="kolichestvo-nazad">&#9664;</div>
                                                    ${kolichestvo}
                                                <div class="kolichestvo-vpered">&#9654;</div>
                                            </div>

                                        </div>

                                        <div class="tovar-itog-price">Price:
                                            <div class="itog-price">${ob.price}</div>
                                        </div>

                                        <div class="tovar-itogo">Итого:
                                            <div class="itogo">${(kolichestvo * parseFloat(ob.price)).toFixed(2)}</div>
                                        </div>

                                    </div>

                                    <div class="tovar-udalit">&#10006;</div>

                                    </div>
                                    `;

                    //вывести tovar
                    let glTextKorzinki = poisk(".gl-text-korzinki");
                    glTextKorzinki.insertAdjacentHTML("afterend", tovar);

                    //вывести кол-во на корзинку сверху:
                    function ikonka() {

                        let kolvo = poisk(".kolvo");
                        let kolvoModalka = poisk(".produkt .kolvo");

                        let [...tovars] = document.querySelectorAll(".tovar");

                        kolvo.innerText = tovars.length;
                        kolvoModalka.innerText = tovars.length;

                        console.log(kolvoModalka)
                    }

                    ikonka(); //обновлять иконку

                    // выбрать количество 
                    let kolichestvoNazad = poisk(".kolichestvo-nazad");
                    let kolichestvoVpered = poisk(".kolichestvo-vpered");

                    kolichestvoNazad.addEventListener("click", () => {

                        kolichestvo--;

                        if (kolichestvo < 1) {
                            kolichestvo = 1;
                        }
                        console.log(kolichestvo);

                    })

                    kolichestvoVpered.addEventListener("click", () => {

                        kolichestvo++;
                        console.log(kolichestvo);
                    })

                    //вывести ВСЕГО  
                    let vsego = poisk(".vsego");
                    let span = vsego.children[0];
                    let [...itog] = document.querySelectorAll(".itogo");

                    let res = itog.map((element) => {
                        let g = element.innerText;
                        return parseFloat(g);
                    })

                    let resultat = res.reduce((a, b) => {
                        return a + b;
                    });

                    span.innerText = `${resultat.toFixed(2)} $`;

                    // удалить товар
                    let tovarUdalit = poisk(".tovar-udalit");
                    tovarUdalit.addEventListener("click", () => {

                        poisk(".tovar").remove();

                        //обновить иконку  
                        ikonka();

                        //обновить ВСЕГО  
                        let vsego = poisk(".vsego");
                        let span = vsego.children[0];
                        let [...itog] = document.querySelectorAll(".itogo");

                        let res = itog.map((element) => {
                            let g = element.innerText;
                            return parseFloat(g);
                        })

                        if (res.length == 0) {
                            span.innerText = "00.00 $"
                        }

                        if (res.length > 0) {
                            let resultat = res.reduce((a, b) => {
                                return a + b;
                            });

                            span.innerText = `${resultat.toFixed(2)} $`;
                        }

                    });

                })

                //модальное окно корзина
                let korzinaPokupok = poisk(".korzina-pokupok");

                //открыть
                korzina.addEventListener("click", () => {
                    korzinaPokupok.classList.add("displayBlock");
                })

                //открыть корзинку при нажатии верхней корзинки
                let korzinaItigo = poisk(".produkt .korzina-itigo");

                korzinaItigo.addEventListener("click", () => {
                    korzinaPokupok.classList.add("displayBlock");
                })


                //закрыть
                let blockZakrit = poisk(".block-gl-text-korzinki-zakrit");
                let prodol = poisk(".prodol");

                blockZakrit.addEventListener("click", () => {
                    korzinaPokupok.classList.remove("displayBlock");
                })

                prodol.addEventListener("click", () => {
                    korzinaPokupok.classList.remove("displayBlock");
                })

                //закрыть большую карточку
                let nazad = poisk(".nazad");
                nazad.addEventListener("click", () => {
                    produkt.classList.remove("displayBlock");
                })

                //console.log(poiskObj)
            })
        })

        //открыть корзинку при нажатии верхней корзинки

        let korzinaPokupok = poisk(".korzina-pokupok");
        let korzinaItigoHeader = poisk("header .korzina-itigo");

        korzinaItigoHeader.addEventListener("click", () => {
            korzinaPokupok.classList.add("displayBlock");
        })

        //закрыть
        let blockZakrit = poisk(".block-gl-text-korzinki-zakrit");
        let prodol = poisk(".prodol");

        blockZakrit.addEventListener("click", () => {
            korzinaPokupok.classList.remove("displayBlock");
        })

        prodol.addEventListener("click", () => {
            korzinaPokupok.classList.remove("displayBlock");
        })
    });


// РАБОТА С КАТАЛОГОМ (2 страницa):

//вывод карточек в каталоге (10 шт на 1 страницу)
function vivestiSfiltrom(size, color) {

    let url = "../js/shorts.json";
    let x = fetch(url);

    let loading = poisk(".loading");
    loading.classList.add("displayBlock");

    x
        .then((otvet) => {
            loading.classList.remove("displayBlock");
            return otvet.json();
        })
        .then((arr) => {
            //console.log(arr)

            let katalog = poisk(".katalog");
            let [...kartochki] = document.querySelectorAll(".kartochka-kataloga");
            let newArr = [];

            //если отфильтровать по размеру  
            if (size !== null && color == null) {

                //пересмотрим каждый объект
                arr.forEach((obj) => {

                    //отфильтрует массив с размерами
                    let newObj = obj.sizes.filter((razmer) => {
                        return razmer == size;
                    })

                    //и в тех что есть совпадения - вывести в новый массив
                    if (newObj.length > 0) {

                        // вначале удалить предыдущие карточки
                        kartochki.forEach((el) => {
                            el.remove();
                        })

                        newArr.push(obj);
                    }
                })
            }

            //если отфильтровать по цвету
            if (size == null && color !== null) {

                //пересмотрим каждый объект
                arr.forEach((obj) => {

                    // отфильтрует массив с цветами
                    let newObj = obj.colors.filter((svet) => {
                        return svet == color;
                    })

                    //и в тех что есть совпадения - вывести в новый массив
                    if (newObj.length > 0) {

                        // вначале удалить предыдущие карточки
                        kartochki.forEach((el) => {
                            el.remove();
                        })

                        newArr.push(obj);
                    }
                })
            }

            //если отфильтровать и по цвету и по размеру
            if (size !== null && color !== null) {

                //пересмотрим каждый объект
                arr.forEach((obj) => {

                    //отфильтрует массив с размерами
                    let newObj = obj.sizes.filter((razmer) => {
                        return razmer == size;
                    })

                    // отфильтрует массив с цветами
                    let newOb = obj.colors.filter((svet) => {
                        return svet == color;
                    })

                    //и в тех что есть совпадения - вывести в новый массив
                    if (newObj.length > 0 && newOb.length > 0) {

                        // вначале удалить предыдущие карточки
                        kartochki.forEach((el) => {
                            el.remove();
                        })

                        newArr.push(obj);
                    }
                })
            }

            //если нет фильтров - вывод всех карточек:
            if (size == null && color == null) {
                arr.forEach((obj) => {

                    // вначале удалить предыдущие карточки
                    kartochki.forEach((el) => {
                        el.remove();
                    })

                    newArr.push(obj);
                })
            }


            //вывести карточки каталога из нового массива
            function vivestiStr(ot, doo) {

                // вначале удалить предыдущие карточки
                let [...kartochki] = document.querySelectorAll(".kartochka-kataloga");
                kartochki.forEach((el) => {
                    el.remove();
                })

                newArr.slice(ot, doo).forEach((obj) => {

                    //перебрать массив со всеми цветами и вывести в элементе
                    let divColor = obj.colors.map((color) => {
                        let div = document.createElement("div");
                        div.classList.add("variant-color");
                        div.style.backgroundColor = color;
                        return div.outerHTML;
                    })

                    //вывести карточку
                    let kartochka = `
                    <div class="kartochka-kataloga">
    
                        <div class="artik">${obj.artikul}</div>

                        <div class="block-img-kataloga">
                            <img class="img-kataloga" src="${obj.photo[0]}" alt="short">
                        </div>
    
                        <div class="name-kartochki-kataloga">${obj.name}</div>
    
                        <div class="zvezds">
                            <div class="zvezda_one">&#9734;</div>
                            <div class="zvezda_two">&#9734;</div>
                            <div class="zvezda_tre">&#9734;</div>
                            <div class="zvezda_for">&#9734;</div>
                            <div class="zvezda_five">&#9734;</div>
                        </div>
    
                        <div class="czena">As low as <span class="pricess">$</span><span
                                class="price pricess">${(obj.price).toFixed(2)}</span></div>
    
                        <div class="variants-color">
                        ${divColor.join("")}
                        </div>
                        
                        <button class="v-korzinky">&#128722; ADD TO BAG</button>
    
                    </div>
                    `;
                    katalog.insertAdjacentHTML("afterbegin", kartochka);
                })


                // РАБОТА С БОЛЬШОЙ КАРТОЧКОЙ: 

                let [...vKorzinky] = document.querySelectorAll(".v-korzinky");
                let produkt = poisk(".produkt"); // большая карточка                

                vKorzinky.forEach((button) => {

                    button.addEventListener("click", () => {

                        produkt.classList.add("displayBlock");  //открыть большую карточку

                        //найти родителя карточки, где была нажата кнопка корзинки
                        let roditel = button.parentElement;

                        // добраться до артикула
                        let arttik = roditel.firstElementChild.innerText;

                        //найти и вывести данную карточку среди массива
                        let poiskObj = undefined;

                        newArr.forEach((obj) => {

                            if (obj.artikul == arttik) {
                                poiskObj = obj;
                            }

                        })

                        //вывести данные из найденной карточки в большую карточку
                        //console.log(poiskObj);

                        // добавить фото
                        let photoOne = poisk(".photo-one");
                        photoOne.setAttribute("src", poiskObj.photo[0]);

                        let photoTwo = poisk(".card-imgs-two");
                        photoTwo.setAttribute("src", poiskObj.photo[1]);

                        let photoTre = poisk(".card-imgs-tre");
                        photoTre.setAttribute("src", poiskObj.photo[2]);

                        //активировать выбранное фото:
                        let twoCards = poisk(".two-cards");
                        twoCards.addEventListener("click", (el) => {

                            if (!el.target.closest("img")) {
                                return
                            }

                            let [...img] = twoCards.children;

                            img.forEach((element) => {
                                element.classList.remove("activ-img");
                            });

                            el.target.classList.add("activ-img");

                            poisk(".card-imgs-one").firstElementChild.src = el.target.src;  //заменить фото
                        })

                        // добавить артикул 
                        let artikulKod = poisk(".artikul-kod");
                        artikulKod.innerText = poiskObj.artikul;

                        // добавить цвета 
                        let colors = poisk(".colors");

                        //перебрать массив со всеми цветами и вывести в элементе
                        let divColor = poiskObj.colors.map((color) => {
                            let div = document.createElement("div");
                            div.classList.add("color");
                            div.style.backgroundColor = color;
                            return div.outerHTML;
                        })

                        colors.innerHTML = divColor.join("");

                        // добавить имя
                        let nameProdukt = poisk(".name_produkt");
                        nameProdukt.innerText = poiskObj.name;

                        // добавить цену 
                        let price = poisk(".prices .price");
                        price.innerText = poiskObj.price.toFixed(2);

                        // добавить размеры 
                        let sizes = poisk(".sizes");

                        //перебрать массив со всеми размерами и вывести в элементе
                        let divSize = poiskObj.sizes.map((size) => {
                            let div = document.createElement("div");
                            div.classList.add("size");
                            div.innerText = size;
                            return div.outerHTML;
                        })

                        sizes.innerHTML = divSize.join("");

                        // добавить отзовы
                        let otzovs = poisk(".otzovs");
                        let newblock = [];

                        //перебрать массив с отзывами и вывести в элементе
                        let divOtzov = poiskObj.otzovi.map((otzov) => {

                            let div = document.createElement("div");
                            div.classList.add("otzov_");
                            div.innerText = otzov;

                            let otzovZvezda = document.createElement("div");
                            otzovZvezda.classList.add("otzov-zvezda");

                            let zvezda = document.createElement("div");
                            zvezda.classList.add("zvezda");
                            zvezda.innerHTML = `&#9734;&#9734;&#9734;&#9734;&#9734;`

                            otzovZvezda.insertAdjacentHTML("afterbegin", div.outerHTML);
                            otzovZvezda.insertAdjacentHTML("afterbegin", zvezda.outerHTML);

                            return newblock.push(otzovZvezda.outerHTML);
                        })

                        //вывести отзывы
                        let newblockOne = newblock.slice(0, 8); //первые 8 шт
                        let newblockTwo = newblock.slice(8);  //остальные

                        otzovs.innerHTML = newblockOne.join("");

                        // пагинация страниц
                        let paginaciyaOne = poisk(".paginaciya-one");
                        let paginaciyaTwo = poisk(".paginaciya-two");

                        //вначале удалить предыдущие
                        paginaciyaOne.addEventListener("click", () => {

                            let [...otzovi] = document.querySelectorAll(".otzov-zvezda");

                            otzovi.forEach((el) => {
                                el.remove();
                            })
                        })

                        paginaciyaTwo.addEventListener("click", () => {

                            let [...otzovi] = document.querySelectorAll(".otzov-zvezda");

                            otzovi.forEach((el) => {
                                el.remove();
                            })
                        })

                        //затем добавить необходимые
                        paginaciyaOne.addEventListener("click", () => {

                            paginaciyaOne.classList.add("paginaciya-activ");
                            paginaciyaOne.classList.remove("paginaciya-passiv");

                            paginaciyaTwo.classList.add("paginaciya-passiv");
                            paginaciyaTwo.classList.remove("paginaciya-activ");

                            otzovs.insertAdjacentHTML("afterbegin", newblockOne.join(""));

                        })

                        paginaciyaTwo.addEventListener("click", () => {

                            paginaciyaOne.classList.add("paginaciya-passiv");
                            paginaciyaOne.classList.remove("paginaciya-activ");

                            paginaciyaTwo.classList.add("paginaciya-activ");
                            paginaciyaTwo.classList.remove("paginaciya-passiv");

                            otzovs.insertAdjacentHTML("afterbegin", newblockTwo.join(""));

                        })

                        //добавить количество отзывов 
                        let otzovi = poisk(".otzovi");
                        otzovi.innerText = divOtzov.length;

                        //РАБОТА С КОРЗИНКОЙ                   

                        //объект корзинка для localStorage:
                        let object = {
                            photo: undefined,
                            name: undefined,
                            color: undefined,
                            size: undefined,
                            price: undefined,
                        }

                        // выбор размера:
                        sizes.children[0].classList.add("activ");

                        sizes.addEventListener("click", (el) => {

                            if (!el.target.closest(".size")) {
                                return
                            }

                            let [...size] = sizes.children;

                            size.forEach((element) => {
                                element.classList.remove("activ");
                            });

                            el.target.classList.add("activ");
                        })

                        // выбор цвета:
                        colors.children[0].classList.add("color-activ");

                        colors.addEventListener("click", (el) => {

                            if (!el.target.closest(".color")) {
                                return
                            }

                            let [...color] = colors.children;

                            color.forEach((element) => {
                                element.classList.remove("color-activ");
                            });

                            el.target.classList.add("color-activ");
                        })

                        // сохранить данные в объект 
                        let korzina = poisk(".korzina");


                        korzina.addEventListener("click", () => {

                            let nameProdukt = poisk(".name_produkt");
                            object.name = nameProdukt.innerText;

                            let cardImgsOne = poisk(".photo-one");
                            object.photo = cardImgsOne.src;

                            let price = poisk(".prices .price");
                            object.price = price.innerText;

                            let sizeActiv = poisk(".activ");
                            object.size = sizeActiv.innerText;

                            let colorActiv = poisk(".color-activ");
                            let cvet = getComputedStyle(colorActiv)["backgroundColor"];
                            object.color = cvet;

                            //console.log(object); //вывести объект для localStorage
                        })

                        //из объекта переобразовать и сохранить в localStorage
                        korzina.addEventListener("click", () => {
                            localStorage.tovar = JSON.stringify(object);
                        })

                        //вывести корзинку уже с данными из localStorage
                        korzina.addEventListener("click", () => {

                            //вывести  из localStorage                            
                            let ob = JSON.parse(localStorage.tovar);
                            console.log(ob);

                            let tovar = `
                            <div class="tovar">
                        
                            <div class="tovar-photo">
                                <div class="block-photo">
                                    <img class="photo" src= ${ob.photo} alt="short">
                                </div>
                            </div>
                        
                            <div class="tovar-name">
                                <div>Название: <span>${ob.name}</span></div>
                        
                                <div class="vibor">
                        
                                    <div class="vibor">Цвет:&nbsp;&nbsp;                
                                        <div class="color" style="background-color:${ob.color};"></div> 
                                    </div>
                        
                                    <div class="vibor">Размер:&nbsp;&nbsp; 
                                        <div class="size">${ob.size}</div>
                                    </div>
                        
                                </div>
                            </div>
                        
                            <div class="tovar-itog">
                        
                                <div class="tovar-itog-kolichestvo">Kол-во:
                        
                                    <div class="kolichestvo">
                                        <div class="kolichestvo-nazad">&#9664;</div>
                                        <div class="kol"></div>
                                        <div class="kolichestvo-vpered">&#9654;</div>
                                    </div>
                        
                                </div>
                        
                                <div class="tovar-itog-price">Price:
                                    <div class="itog-price">${ob.price}</div>
                                </div>
                        
                                <div class="tovar-itogo">Итого:
                                    <div class="itogo"></div>
                                </div>
                        
                            </div>
                        
                            <div class="tovar-udalit">&#10006;</div>
                        
                            </div>
                            `;

                            //вывести tovar
                            let glTextKorzinki = poisk(".gl-text-korzinki");
                            glTextKorzinki.insertAdjacentHTML("afterend", tovar);

                            //вывести кол-во на корзинку сверху:
                            function ikonka() {

                                let kolvo = poisk(".kolvo");
                                let kolvoModalka = poisk(".produkt .kolvo");
                                let [...tovar] = document.querySelectorAll(".tovar");

                                kolvo.innerText = tovar.length;
                                kolvoModalka.innerText = tovar.length;
                            }

                            ikonka(); //обновить иконку

                            // выбрать количество, математическая операция и вывод: итого и ВСЕГО
                            let kolichestvoNazad = poisk(".kolichestvo-nazad");
                            let kolichestvoVpered = poisk(".kolichestvo-vpered");
                            let kol = poisk(".kol");
                            let itogo = poisk(".itogo");
                            let itog = [];

                            let k = 1;
                            kol.innerText = k;
                            ikonka();

                            itogo.innerText = `${(k * parseFloat(ob.price)).toFixed(2)}`;
                            itog.push(itogo.innerText);

                            //вывести ВСЕГО  
                            let vsego = poisk(".vsego");
                            let span = vsego.children[0];
                            let [...ito] = document.querySelectorAll(".itogo");

                            let res = ito.map((element) => {
                                let g = element.innerText;
                                return parseFloat(g);
                            })

                            let resultat = res.reduce((a, b) => {
                                return a + b;
                            });

                            span.innerText = `${resultat.toFixed(2)} $`;

                            kolichestvoNazad.addEventListener("click", () => {

                                k--;

                                if (k < 1) {
                                    k = 1;
                                }

                                kol.innerText = k;
                                itogo.innerText = `${(k * parseFloat(ob.price)).toFixed(2)}`;
                                itog.push(itogo.innerText);

                                //вывести ВСЕГО  
                                let vsego = poisk(".vsego");
                                let span = vsego.children[0];
                                let [...ito] = document.querySelectorAll(".itogo");
                                //console.log(itog)

                                ikonka();

                                let res = ito.map((element) => {
                                    let g = element.innerText;
                                    return parseFloat(g);
                                })

                                let resultat = res.reduce((a, b) => {
                                    return a + b;
                                });

                                span.innerText = `${resultat.toFixed(2)} $`;
                            })

                            kolichestvoVpered.addEventListener("click", () => {

                                k++;
                                kol.innerText = k;
                                itogo.innerText = `${(k * parseFloat(ob.price)).toFixed(2)}`;
                                itog.push(itogo.innerText);

                                //вывести ВСЕГО  
                                let vsego = poisk(".vsego");
                                let span = vsego.children[0];
                                let [...ito] = document.querySelectorAll(".itogo");
                                //console.log(itog)

                                ikonka();

                                let res = ito.map((element) => {
                                    let g = element.innerText;
                                    return parseFloat(g);
                                })

                                let resultat = res.reduce((a, b) => {
                                    return a + b;
                                });

                                span.innerText = `${resultat.toFixed(2)} $`;
                            })

                            // удалить товар
                            let tovarUdalit = poisk(".tovar-udalit");
                            tovarUdalit.addEventListener("click", () => {

                                tovarUdalit.parentElement.remove();

                                //обновить иконку  
                                ikonka();

                                //обновить ВСЕГО  
                                let vsego = poisk(".vsego");
                                let span = vsego.children[0];
                                let [...ito] = document.querySelectorAll(".itogo");

                                let res = ito.map((element) => {
                                    let g = element.innerText;
                                    return parseFloat(g);
                                })

                                if (res.length == 0) {
                                    span.innerText = "00.00 $"
                                }

                                if (res.length > 0) {
                                    let resultat = res.reduce((a, b) => {
                                        return a + b;
                                    });

                                    span.innerText = `${resultat.toFixed(2)} $`;
                                }

                            });

                        })

                        //модальное окно корзина
                        let korzinaPokupok = poisk(".korzina-pokupok");

                        //открыть
                        korzina.addEventListener("click", () => {
                            korzinaPokupok.classList.add("displayBlock");
                        })

                        //открыть корзинку при нажатии верхней корзинки
                        let korzinaItigo = poisk(".produkt .korzina-itigo");

                        korzinaItigo.addEventListener("click", () => {
                            korzinaPokupok.classList.add("displayBlock");
                        })


                        //закрыть
                        let blockZakrit = poisk(".block-gl-text-korzinki-zakrit");
                        let prodol = poisk(".prodol");

                        blockZakrit.addEventListener("click", () => {
                            korzinaPokupok.classList.remove("displayBlock");
                        })

                        prodol.addEventListener("click", () => {
                            korzinaPokupok.classList.remove("displayBlock");
                        })

                        //закрыть большую карточку
                        let nazad = poisk(".nazad");
                        nazad.addEventListener("click", () => {
                            produkt.classList.remove("displayBlock");
                        })

                        //console.log(poiskObj)
                    })
                })

                //открыть корзинку при нажатии верхней корзинки

                let korzinaPokupok = poisk(".korzina-pokupok");
                let korzinaItigoHeader = poisk("header .korzina-itigo");

                korzinaItigoHeader.addEventListener("click", () => {
                    korzinaPokupok.classList.add("displayBlock");
                })

                //закрыть
                let blockZakrit = poisk(".block-gl-text-korzinki-zakrit");
                let prodol = poisk(".prodol");

                blockZakrit.addEventListener("click", () => {
                    korzinaPokupok.classList.remove("displayBlock");
                })

                prodol.addEventListener("click", () => {
                    korzinaPokupok.classList.remove("displayBlock");
                })
            }

            // РАБОТА С ПОИСКОМ (2 страницa):

            let poiskPoKatalogy = poisk(".poisk-po-katalogy");

            poiskPoKatalogy.addEventListener("search", () => {

                newArr = [];
                let f = [];

                arr.forEach((obj) => {

                    let regulyarka = new RegExp(poiskPoKatalogy.value);

                    if (obj.name.search(regulyarka) >= 0) {

                        f.push(obj);
                        //console.log(obj); // сколько карточек нашлось?
                    }
                })

                f.forEach((el) => {
                    newArr.push(el);
                })

                vivestiStr();
                //console.log(newArr);
            })

            // РАБОТА С ПАГИНАЦИЕЙ

            //показать первую страницу каталога со всеми карточками
            vivestiStr(0, 10);

            // пагинация страниц
            let pagOne = poisk(".pag-one");
            let pagTwo = poisk(".pag-two");
            let pagTre = poisk(".pag-tre");
            let pagFor = poisk(".pag-for");
            let pagFive = poisk(".pag-five");

            //при активации пагинации - удалить предыдущие карточки
            let paginaciya = poisk(".paginaciya");

            paginaciya.addEventListener("click", (el) => {

                if (!el.target.closest("button")) {
                    return
                }

                let [...kartochki] = document.querySelectorAll(".kartochka-kataloga");

                kartochki.forEach((el) => {
                    el.remove();
                })

                //console.log(kartochki);
            })

            //активировать пагинацию:
            paginaciya.addEventListener("click", (el) => {

                if (!el.target.closest("button")) {
                    return
                }

                let [...pag] = paginaciya.children;

                pag.forEach((element) => {
                    element.classList.remove("paginaciya-activ");
                    element.classList.add("paginaciya-passiv");
                });

                el.target.classList.remove("paginaciya-passiv");
                el.target.classList.add("paginaciya-activ");

                // вывести необходимую страницу

                //если активна первая страница: 
                if (el.target.innerText == 1 && el.target.classList.contains("paginaciya-activ")) {
                    vivestiStr(0, 10);
                    pagOne.innerText = 1; //activ
                    pagTwo.innerText = 2;
                    pagTre.innerText = 3;
                    pagFor.innerText = 4;
                    pagFive.innerText = "❯";
                }

                if (el.target.innerText == "❯" && el.target.classList.contains("paginaciya-activ")) {
                    vivestiStr(51, 60);
                    pagOne.innerText = "❮";
                    pagTwo.innerText = 2;
                    pagTre.innerText = 3;
                    pagFor.innerText = 4;
                    pagFive.innerText = 5;  //activ
                }

                if (el.target.innerText == "❮" && el.target.classList.contains("paginaciya-activ")) {
                    vivestiStr(0, 10);
                    pagOne.innerText = 1; //activ
                    pagTwo.innerText = 2;
                    pagTre.innerText = 3;
                    pagFor.innerText = 4;
                    pagFive.innerText = "❯";
                }

                if (el.target.innerText == 2 && el.target.classList.contains("paginaciya-activ")) {
                    vivestiStr(10, 20);
                    pagOne.innerText = 1;
                    pagTwo.innerText = 2; //activ
                    pagTre.innerText = 3;
                    pagFor.innerText = 4;
                    pagFive.innerText = "❯";
                }

                if (el.target.innerText == 3 && el.target.classList.contains("paginaciya-activ")) {
                    vivestiStr(20, 30);
                    pagOne.innerText = "❮";
                    pagTwo.innerText = 2;
                    pagTre.innerText = 3; //activ
                    pagFor.innerText = 4;
                    pagFive.innerText = "❯";
                }

                if (el.target.innerText == 4 && el.target.classList.contains("paginaciya-activ")) {
                    vivestiStr(30, 40);
                    pagOne.innerText = "❮";
                    pagTwo.innerText = 2;
                    pagTre.innerText = 3;
                    pagFor.innerText = 4; //activ
                    pagFive.innerText = 5;
                }

                if (el.target.innerText == 5 && el.target.classList.contains("paginaciya-activ")) {
                    vivestiStr(40, 50);
                    pagOne.innerText = "❮";
                    pagTwo.innerText = 2;
                    pagTre.innerText = 3;
                    pagFor.innerText = 4;
                    pagFive.innerText = 5;  //activ
                }
            })

            //console.log(newArr); //показать новый массив карточек в каталоге        
        });
}

// РАБОТА С ФИЛЬТРАМИ 

//для передачи информации в функцию вывода карточек с выбранными фильтрами
let filterVibor = {
    razmer: null,
    color: null
};

//показать первую страницу каталога со всеми карточками
vivestiSfiltrom(filterVibor.razmer, filterVibor.color);

//filter
let strelkaSizeVniz = poisk(".strelka-size-vniz");
let strelkaSizeVverh = poisk(".strelka-size-vverh");

//size
let [...filterViborSize] = document.querySelectorAll(".filter-vibor-size");

//расскрыть
strelkaSizeVniz.addEventListener("click", () => {

    strelkaSizeVniz.classList.add("displayNone");
    strelkaSizeVverh.classList.add("displayBlock");

    filterViborSize.forEach((el) => {
        el.classList.add("displayBlock");
    })
})

//спрятать
strelkaSizeVverh.addEventListener("click", () => {

    strelkaSizeVniz.classList.remove("displayNone");
    strelkaSizeVverh.classList.remove("displayBlock");

    filterViborSize.forEach((el) => {
        el.classList.remove("displayBlock");
    })
})

// выбор size:
let blockFilterSize = poisk(".block-filter-size");

blockFilterSize.addEventListener("click", (el) => {

    //активировать фильтр

    if (!el.target.closest(".filter-vibor-size")) {
        return;
    }

    let [...size] = blockFilterSize.children;

    size.forEach((element) => {
        element.classList.remove("activ");
    });

    el.target.classList.add("activ");

    //передать инфо функции для вывода карточек

    filterVibor.razmer = el.target.innerText;
    vivestiSfiltrom(filterVibor.razmer, filterVibor.color);
    console.log(filterVibor);
})

//color
let strelkaColorVniz = poisk(".strelka-color-vniz");
let strelkaColorVverh = poisk(".strelka-color-vverh");
let [...filterViborColor] = document.querySelectorAll(".filter-vibor-color");

//расскрыть
strelkaColorVniz.addEventListener("click", () => {

    strelkaColorVniz.classList.add("displayNone");
    strelkaColorVverh.classList.add("displayBlock");

    filterViborColor.forEach((el) => {
        el.classList.add("displayBlock");
    })
})

//спрятать
strelkaColorVverh.addEventListener("click", () => {

    strelkaColorVniz.classList.remove("displayNone");
    strelkaColorVverh.classList.remove("displayBlock");

    filterViborColor.forEach((el) => {
        el.classList.remove("displayBlock");
    })
})

// выбор color:
let blockFilterColor = poisk(".block-filter-color");

blockFilterColor.addEventListener("click", (el) => {

    //активировать фильтр

    if (!el.target.closest(".filter-vibor-color")) {
        return;
    }

    let [...color] = blockFilterColor.children;

    color.forEach((element) => {
        element.classList.remove("activ");
    });

    el.target.classList.add("activ");

    //передать инфо функции для вывода карточек 

    filterVibor.color = el.target.dataset.color;
    vivestiSfiltrom(filterVibor.razmer, filterVibor.color);
    console.log(filterVibor);
})

//Очистить фильтра
let clearFilter = poisk(".clear-filter");
clearFilter.addEventListener("click", () => {

    //деактивировать фильтра

    filterViborColor.forEach((el) => {
        el.classList.remove("activ");
    })

    filterViborSize.forEach((el) => {
        el.classList.remove("activ");
    })

    //передать инфо функции для вывода карточек

    filterVibor.razmer = null;
    filterVibor.color = null;
    vivestiSfiltrom(filterVibor.razmer, filterVibor.color);
    console.log(filterVibor);
})
