/*
Домашнее задание: №16
Создать генератор HTML (словно Emmet в VSCode):
+ вспомнить, разобрать как работает метод .exec() в регулярных выражениях
+ сделать так, чтобы при вводе: # - получалось вводить <div id=""></div>
+ сделать так, чтобы при вводе: . - получалось вводить <div class=""></div>
+ сделать так, чтобы при вводе: div{текст} - получалось вводить <div>текст</div>
+ сделать так, чтобы при вводе: div>p>a - получалось вводить <div><p><a></a></p></div>
Тема: регулярные выражения. Создание элемента (при сборке)
*/

let vvod = document.querySelector(".vvod");
let value = document.querySelector(".value");

// Сбор элемента. Отображение созданного элемента в блоке вывода. 
function createElement(tegName, className, idName, text) {

    let element;

    //создание тега
    if (tegName == "") {
        value.innerText = "";
        return;
    } else {
        element = document.createElement(tegName);
    }

    //создание стиля 
    if (className !== undefined) {
        // element.classList.add(className); - через такой метод в консоле появляются ошибки
        element.className = className;
    }

    //создание id
    if (idName !== undefined) {
        element.id = idName;
    }

    //создание текста
    if (text !== undefined) {
        element.innerText = text;
    }

    value.innerText = element.outerHTML;
}

//ввод данных
vvod.addEventListener("input", () => {

    if (/[.]{1}/.test(vvod.value)) { //если проставлена точка - для создания стиля

        let rez = /[.]{1}/.exec(vvod.value);

        let arrValue = vvod.value.split(/[.]{1}/); //удаляем символ и превращ. текст в массив

        if (rez.index === 0) {  //если начинается с точки
            createElement("div", arrValue[1]);
        } else {
            createElement(arrValue[0], arrValue[1]);
        }

    } else if (/[#]{1}/.test(vvod.value)) { //если проставлено # - для создания id

        let rez = /[#]{1}/.exec(vvod.value);

        let arrValue = vvod.value.split(/[#]{1}/); //удаляем символ и превращ. текст в массив

        if (rez.index === 0) {  //если начинается с #
            createElement("div", undefined, arrValue[1]);
        } else {
            createElement(arrValue[0], undefined, arrValue[1]);
        }

    } else if (/[{}]/.test(vvod.value)) { //если проставлены {} - для создания текста

        let rez = /[{}]/.exec(vvod.value);

        let arrValue = vvod.value.split(/[{}]/); //удаляем символы и превращ. текст в массив

        if (rez.index === 0) {  //если начинается с {}
            createElement("div", undefined, undefined, arrValue[1]);
        } else {
            createElement(arrValue[0], undefined, undefined, arrValue[1]);
        }

    } else if (/[>]+/.test(vvod.value)) { //если проставлены > (от одного до трёх) - для создания вложенностей элементов

        let arrValue = vvod.value.split(/[>]+/); //удаляем символы и превращ. текст в массив

        if (arrValue[1] === "") { //если проставлено >

            document.createElement(arrValue[0]);
        }

        if (arrValue[1] !== "") { //если проставлено >>

            let oneElement = document.createElement(arrValue[0]);
            let twoElement = document.createElement(arrValue[1]);

            //oneElement.firstElementChild = twoElement; // - не получилось             
            //oneElement.insertAdjacentHTML("beforeend", twoElement); // - не получилось 
            //oneElement.appendChild(twoElement) - не получилось 

            oneElement.insertAdjacentElement("beforeend", twoElement);

            value.innerText = oneElement.outerHTML;
        }

        if (arrValue[2]) { //если проставлено >>>

            let a = document.createElement(arrValue[0]);
            let b = document.createElement(arrValue[1]);
            let c = document.createElement(arrValue[2]);

            a.insertAdjacentElement("beforeend", b);
            b.insertAdjacentElement("beforeend", c);

            value.innerText = a.outerHTML;
        }
    }
    else {
        // ну и если просто текст (просто теги)
        createElement(vvod.value);
    }

    //если пусто в поле ввода - очистить блок вывода:
    if (vvod.value.length == 0) {
        createElement("");
    }
})



