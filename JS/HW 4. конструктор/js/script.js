/* Описание:
Необходимо решить задачи. Результаты выводить в console.log()
Тема: Конструктор. */

/* Задача № 1
Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: 
receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". 
Метод getNumber повертає номер телефону. 
Викликати ці методи кожного з об'єктів.*/

console.log(`Задача № 1: `)

function Phone(number, model, weight, name) {
    this.number = number;
    this.model = model;
    this.weight = weight;
    this.name = name;
}

Phone.prototype.receiveCall = function () {
    return console.log(`Телефонує: ${this.name}`);
}

Phone.prototype.getNumber = function () {
    return this.number
}

let dannie = new Phone(911, "Siemens S10", 100);
console.log(dannie);
let dannie2 = new Phone(101, "Sony Ericsson T610", 200);
console.log(dannie2);
let dannie3 = new Phone(104, "Nokia 5110", 300.50);
console.log(dannie3);

let opros = new Phone();
opros.name = (prompt(`введите имя, кто будет звонить`));
opros.receiveCall();

opros.number = `333-33-33`;
console.log(`по номеру телефона: ${opros.getNumber()}`);

/* Задача № 2
Написати функцію filterBy(), яка прийматиме 2 аргументи. 
Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, 
які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. 

Тобто якщо передати масив ['hello', 'world', 23, '23', null], 
і другим аргументом передати 'string', 
то функція поверне масив [23, null].*/

console.log(`Задача № 2: `)

let tetradi = [`клетка`, 14, `линейка`, 24, `в косую`, 60];
console.log(`старый массив: ${tetradi.join(`, `)}`);

// 1. Вариант решения:
console.log(`1. Вариант решения: `)

function filterBy(massiv, tipDannih) {

    console.log(`проверка: является ли tip - числом? : ${typeof (tipDannih)}`);

    return massiv.map(el => {

        if (typeof el !== typeof tipDannih) {
            return el;
        }
    })
}

let rez = filterBy(tetradi, 888);
console.log(`проверка: является ли rez - массивом? : ${Array.isArray(rez)}`);
console.log(`вывод нового массива без чисел: ${rez.join(` `)}`);

// 2. Вариант решения:
console.log(`2. Вариант решения: `)

let tetradi2 = [`клетка`, 14, `линейка`, 24, `в косую`, 60];
const gg = "number"; // если ввести "string" - получится вывести: только числа : [14, 24, 60] )

function filterBy2(massiv, tipDannih) {

    let ccc = [];

    massiv.forEach(el => {

        if (typeof el !== tipDannih) {
            ccc.push(el);
        }

    });

    //или: 
    /* 
    massiv.forEach( el => {

        typeof el !== tipDannih ? ccc.push(el) : false;

    });
    */

    //или: 
    /*     
    без участия второго аргумента:     

    for (let el = 0; el < massiv.length; el++){

        if (typeof massiv[el] === "string"){
            ccc.push(massiv[el])
        }
    }    
    */

    return ccc;
}

console.log(`вывод нового массива без чисел: ${filterBy2(tetradi2, gg)}`);

// 3. Вариант решения:
console.log(`3. Вариант решения: `)

function filterBy3(massiv, tipDannih) {

    return massiv.filter(el => typeof el !== tipDannih);

}

console.log(`вывод нового массива без текстового типа данных:`);
console.log(filterBy3([`клетка`, 14, `линейка`, 24, `в косую`, 60, null], 'string')); 
